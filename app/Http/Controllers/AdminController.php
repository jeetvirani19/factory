<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\AdminModel;
use DB;
use App;

class AdminController extends Controller
{
   
    public function getAllAdminsList( Request $request )
    {
        if (Auth::check()) {
            $this->middleware('auth');
            $admins = DB::table('users')
            ->select('users.*')
            ->whereNotIn('user_type', [0])
            ->get();
            return view('admin.users.user-list' , ['adminList' => $admins]);
        }
        else
        {
            return redirect('/');     
        }
       
    }

    public function getAdminProfile( Request $request )
    {
        if( $request->userId )
        {
            $admin = DB::table('users')
            ->select('users.*')
            ->where('users.id', '=', $request->userId)
            ->get();
            return view('admin.users.user-detail' , ['admin' => $admin[0]]);
        }
        else
        {
            App::abort(404);
        }
    }

    public function editAdminProfile( Request $request )
    {
        if( $request->userId )
        {
            $admin = DB::table('users')
            ->select('users.*')
            ->where('users.id', '=', $request->userId)
            ->get();
            return view('admin.users.edit-user' , ['admin' => $admin[0]]);
        }
        else
        {
            App::abort(404);
        }
    }

    public function updateAdminProfile( Request $request )
    {
        if($request->userId)
        {
            print_r($request->location_id);

           $updateResponse= DB::table('users')
            ->where('id', $request->userId)
            ->update(
                ['name' => $request->first_name,
                'email' => $request->user_email,
                'is_active' => $request->is_active == "on" ? 1 : 0]
            );
            return redirect('manage-admin/'.$request->userId);
        }
        else
        {
            App::abort(404);
        }
       
    }

    public function deleteAdminProfile( Request $request)
    {
        if($request->userId)
        {
            $admin = DB::table('users')
            ->where('id',$request->userId);
             $admin->delete();
             $admins = DB::table('users')
            ->join('locations', 'locations.location_id', '=', 'users.location_id')
            ->select('users.*', 'locations.area')
            ->get();
            return view('admin.users.user-list' , ['adminList' => $admins]);
        }
        else
        {
            App:abort(404);
        }
    }

     public function createAdminProfile( Request $request )
    {
     
            return view('admin.users.create-user');
       
    }
     public function saveAdminProfile( Request $request )
    {
        
        $adminUser = DB::table('users')->insert([
                'name' => $request->first_name,
                'email' => $request->user_email,
                'is_active' => $request->is_active == "on" ? 1 : 0 ,
                'user_type'=>1,
                'is_reset_password_requested'=>0,
                'password' => bcrypt($request->password)]);

            return redirect('manage-admin/'.$request->userId);
    }

    public function deleteAdmin(Request $request)
    {
        if($request->id)
        {
            $deletedFlag = DB::table('users')->where('id', '=',$request->id )->delete();
            if($deletedFlag)
            {
                return redirect('manage-admin'); 
            }
        }
        else
        {
            App::abort(404);
        }
    }

    

}
