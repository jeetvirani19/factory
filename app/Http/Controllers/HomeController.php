<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    var $USER_TABLE = "users";
    var $PRODUCTS_TABLE = "product";
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getDashboard()
    {
                  
        // $countData = DB::select("SELECT 'gh_store' AS 'store', COUNT(*) as count FROM gh_store UNION SELECT 'gh_slider_images' AS 'slider', COUNT(*) as count FROM gh_slider_images UNION SELECT 'gh_category' AS 'cat', COUNT(*) as count FROM gh_category UNION SELECT 'gh_product' AS 'prod', COUNT(*) as count FROM gh_product");
        return view('admin.dashboard');
    }
}
