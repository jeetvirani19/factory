<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\SliderModel;
use DB;
use App;

class InvoiceController extends Controller
{

    var $Invoice_TABLE = "invoice";
    var $PRODUCT_TABLE = "product";

    public function getAllInvoiceList( Request $request )
    {
        if (Auth::check()) {
                $invoices = DB::table($this->Invoice_TABLE)
                ->select('invoice.*')
                ->get();
            return view('admin.invoice.invoice-list' , ['invoiceList' => $invoices]);
        }
    }
    

    public function getInvoiceInfo( Request $request )
    {
        if( $request->invoice_id )
        {
              $invoices = DB::table($this->Invoice_TABLE)
            ->join($this->PRODUCT_TABLE, 'invoice.product_id', '=', 'product.product_id')
            ->select('product.product_name','invoice.*')
            ->where('invoice.invoice_id', '=', $request->invoice_id)
            ->get();
        
            return view('admin.invoice.invoice-detail' , ['invoiceList' => $invoices[0]]);
   
        }
        else
        {
            App::abort(404);
        }
    }


    public function editInvoiceInfo( Request $request )
    {
        if( $request->invoice_id )
        {

            $invoices = DB::table($this->Invoice_TABLE)
            ->select('invoice.*')
            ->where('invoice.invoice_id', '=', $request->invoice_id)
            ->get();

            $products = DB::table('product')
            ->select('product.*')
            ->get();
            return view('admin.invoice.edit-invoice' , ['invoiceList' => $invoices[0] , 'products'=>$products]);
            
           }
        else
        {
            App::abort(404);
        }
    }

    
      public function saveInvoice( Request $request )
    {
        

        $gh_product = DB::table($this->PRODUCT_TABLE)
                 ->select('product.*')
                 ->where('product.product_id','=',$request->product_id)
                 ->get();
        
        if(($gh_product[0]->quantity_in_bundle  - $request->product_qty)>=0){
              $newInvoice = DB::table($this->Invoice_TABLE)->insert([
                'customer_name' => $request->customer_name,
                'product_id' => $request->product_id,
                'product_qty' => $request->product_qty,
                'product_price' => $request->product_price,
                'total' => $request->total]);

              $updateResponse= DB::table($this->PRODUCT_TABLE)
                            ->where('product_id', $request->product_id)
                            ->update([
                                'quantity_in_bundle' => $gh_product[0]->quantity_in_bundle  - $request->product_qty
                                ]
                        
                            );
            
                return redirect('manage-invoice'); 
        }else{
             $products = DB::table('product')
            ->select('product.*')
            ->get();
    
            return view('admin.invoice.create-invoice', ['productList' => $products]);  
        }
    }

       public function createInvoice( Request $request )
        {
            $products = DB::table('product')
            ->select('product.*')
            ->get();
    
            return view('admin.invoice.create-invoice', ['productList' => $products]);   
        }


    public function updateInvoiceInfo( Request $request )
    {
     
                $updateResponse= DB::table($this->Invoice_TABLE)
                            ->where('invoice_id', $request->invoice_id)
                            ->update([
                                'customer_name' => $request->customer_name,
                                'product_qty' => $request->product_qty,
                                'product_price' => $request->product_price,
                                'total' => $request->total
                                ]
                        
                            );
            
          
            return redirect('manage-invoice/'.$request->invoice_id);
      
       
    }


  public function deleteInvoice(Request $request)
    {
        if($request->invoice_id)
        {
            $deletedFlag = DB::table($this->Invoice_TABLE)->where('invoice_id', '=',$request->invoice_id )->delete();
            if($deletedFlag)
            {
                return redirect('manage-invoice'); 
            }
        }
        else
        {
            App::abort(404);
        }
    }    

}
