<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App;

class MachineController extends Controller
{

    var $MACHINE_TABLE = "machines";

    public function getAllMachineList( Request $request )
    {
        if (Auth::check()) {
            $machines = DB::table($this->MACHINE_TABLE)
            ->select('machines.*')
            ->get();
            return view('admin.machine.machine-list' , ['machineList' => $machines]);
        }
    }
    

    public function getMachineInfo( Request $request )
    {
        if( $request->machine_id )
        {
            $machines = DB::table($this->MACHINE_TABLE)
            ->select('machines.*')
            ->where('machines.machine_id', '=', $request->machine_id)
            ->get();
            return view('admin.machine.machine-detail' , ['machineList' => $machines[0]]);
   
        }
        else
        {
            App::abort(404);
        }
    }


    public function editMachineInfo( Request $request )
    {
        if( $request->machine_id )
        {
            $machines = DB::table($this->MACHINE_TABLE)
            ->select('machines.*')
            ->where('machines.machine_id', '=', $request->machine_id)
            ->get();
            return view('admin.machine.edit-machine' , ['machineList' => $machines[0]]);
           }
        else
        {
            App::abort(404);
        }
    }

    
      public function saveMachine( Request $request )
    {
        

        $newMachine = DB::table($this->MACHINE_TABLE)->insert([
                'machine_name' => $request->machine_name,     
                'temperature' => $request->temperature,
                'locking' => $request->locking,
                'pressure' => $request->pressure,
                'scale' => $request->scale]);

          $machines = DB::table($this->MACHINE_TABLE)
            ->select('machines.*')
            ->get();
            return view('admin.machine.machine-list' , ['machineList' => $machines]);
 
    }

       public function createMachine( Request $request )
        {
            return view('admin.machine.create-machine');   
        }


    public function updateMachineInfo( Request $request )
    {
        if($request->machine_id)
        {
      
           $updateResponse= DB::table($this->MACHINE_TABLE)
             ->where('machine_id', $request->machine_id)
            ->update(
                [
                'machine_name' => $request->machine_name,     
                'temperature' => $request->temperature,
                'locking' => $request->locking,
                'pressure' => $request->pressure,
                'scale' => $request->scale,
                'modified_date' => date( 'Y-m-d', strtotime(date('Y-m-d H:i:s')) )]
        
            );
            return redirect('manage-machine/'.$request->machine_id);
        }
        else
        {
            App::abort(404);
        }
       
    }

 public function deleteMachineInfo(Request $request)
    {
        if($request->machine_id)
        {
            $deletedFlag = DB::table($this->MACHINE_TABLE)->where('machine_id', '=',$request->machine_id )->delete();
            if($deletedFlag)
            {
                return redirect('manage-machine'); 
            }
        }
        else
        {
            App::abort(404);
        }
    }   
    

}
