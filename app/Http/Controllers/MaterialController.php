<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App;

class MaterialController extends Controller
{

    var $MATERIAL_TABLE = "materials";

    public function getAllMaterialList( Request $request )
    {
        if (Auth::check()) {
            $materials = DB::table($this->MATERIAL_TABLE)
            ->select('materials.*')
            ->get();
            return view('admin.material.material-list' , ['materialList' => $materials]);
        }
    }
    

    public function getMaterialInfo( Request $request )
    {
        if( $request->material_id )
        {
            $materials = DB::table($this->MATERIAL_TABLE)
            ->select('materials.*')
            ->where('materials.material_id', '=', $request->material_id)
            ->get();
            return view('admin.material.material-detail' , ['materialList' => $materials[0]]);
   
        }
        else
        {
            App::abort(404);
        }
    }


    public function editMaterialInfo( Request $request )
    {
        if( $request->material_id )
        {
            $materials = DB::table($this->MATERIAL_TABLE)
            ->select('materials.*')
            ->where('materials.material_id', '=', $request->material_id)
            ->get();
            return view('admin.material.edit-material' , ['materialList' => $materials[0]]);
           }
        else
        {
            App::abort(404);
        }
    }

    
      public function saveMaterial( Request $request )
    {
        

        $newMaterial = DB::table($this->MATERIAL_TABLE)->insert([
               'name' => $request->name,     
            'seller' => $request->seller,
                'quantity' => $request->quantity,
                'price' => $request->price,
                'quality' => $request->quality,
                'purchase_date' => date( 'Y-m-d', strtotime( $request->dateOfPurchase) )]);

          $materials = DB::table($this->MATERIAL_TABLE)
            ->select('materials.*')
            ->get();
            return view('admin.material.material-list' , ['materialList' => $materials]);
 
    }

       public function createMaterial( Request $request )
        {
            return view('admin.material.create-material');   
        }


    public function updateMaterialInfo( Request $request )
    {
        if($request->material_id)
        {
      
           $updateResponse= DB::table($this->MATERIAL_TABLE)
             ->where('material_id', $request->material_id)
            ->update(
                [
                'name' => $request->name,    
                    'seller' => $request->seller,
                'quantity' => $request->quantity,
                'price' => $request->price,
                'quality' => $request->quality,
                'purchase_date' =>date( 'Y-m-d', strtotime( $request->dateOfPurchase) )]
        
            );
            return redirect('manage-material/'.$request->material_id);
        }
        else
        {
            App::abort(404);
        }
       
    }

 public function deletematerial(Request $request)
    {
        if($request->material_id)
        {
            $deletedFlag = DB::table($this->MATERIAL_TABLE)->where('material_id', '=',$request->material_id )->delete();
            if($deletedFlag)
            {
                return redirect('manage-material'); 
            }
        }
        else
        {
            App::abort(404);
        }
    }   
    

}
