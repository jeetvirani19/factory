<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\ProductModel;
use DB;
use App;

class ProductController extends Controller
{

    var $PRODUCT_TABLE = "product";
    var $USER_TABLE = "users";
 


    public function getAllProductList(Request $request){
         if (Auth::check()) {
                $gh_product = DB::table($this->PRODUCT_TABLE)
                 ->select('product.*')
                 ->get();
           
            return view('admin.product.product-list' , ['productList' => $gh_product]);
         }
    }

    

    

    public function getCategoryInfo( Request $request )
    {
        if( $request->category_id )
        {
            $gh_category = DB::table($this->CATEGORY_TABLE)
            ->select('gh_category.*')
            ->where('gh_category.category_id', '=', $request->category_id)
            ->get();

             $locations = DB::table($this->LOCATION_TABLE)
            ->select('locations.*')
            ->where('locations.location_id','=',$gh_category[0]->location_id)
            ->get();
            return view('admin.product.category-detail' , ['categoryList' => $gh_category[0],'location'=>$locations[0]]);
   
        }
        else
        {
            App::abort(404);
        }
    }

     public function getProductInfo( Request $request )
    {
        if( $request->product_id )
        {
            
            $gh_product = DB::table($this->PRODUCT_TABLE)
            ->select('product.*')
            ->where('product.product_id', '=', $request->product_id)
            
            ->get();
            return view('admin.product.product-detail' , ['productList' => $gh_product[0]]);
            
   
        }
        else
        {
            App::abort(404);
        }
    }


    public function editProductInfo( Request $request )
    {
        if( $request->product_id )
        {

            $gh_product = DB::table($this->PRODUCT_TABLE)
            ->select('product.*')
            ->where('product.product_id', '=', $request->product_id)
            ->get();
            return view('admin.product.edit-product' , ['productList' => $gh_product[0]]);
            
           }
        else
        {
            App::abort(404);
        }
    }

    
      public function saveCategory( Request $request )
    {
        
   
        $adminUser = DB::table($this->CATEGORY_TABLE)->insert([
                'category_name' => $request->category_name,
                'location_id' => $request->location_id,
                'isactive' => $request->is_active == "on" ? 1 : 0]);

        //   $category = DB::table($this->CATEGORY_TABLE)
        //     ->select('gh_category.*')
        //     // ->where('gh_category.location_id','=',Auth::user()->location_id)
        //     ->get();
        //     return view('admin.product.category-list' , ['categoryList' => $category]);
        return redirect('manage-category'.$request->product_id);

    }

     


      public function saveProduct( Request $request )
    {
              if($request->hasFile('product_image')){
                if (Input::file('product_image')->isValid()) {
                    $file = Input::file('product_image');
                    $destination = 'images/uploads'.'/';
                    $ext= $file->getClientOriginalExtension();
                    $mainFilename = str_random(6).date('h-i-s');
                    $file->move($destination, $mainFilename.".".$ext);
                        $adminUser = DB::table($this->PRODUCT_TABLE)->insert([
                                'product_name' => $request->product_name,
                                'product_code' => $request->product_code,
                                'weight' => $request->weight,
                                'image'=> $destination.$mainFilename.".".$ext,
                                'product_description' => $request->product_description,
                                'product_price' => $request->product_price,
                                'bundle_size' => $request->bundle_size,
                                'quantity_in_bundle' => $request->quantity_in_bundle,
                                'isactive' => $request->is_active == "on" ? 1 : 0
                                ],
                                );
                            return redirect('manage-product'); 

                }
            }
       
    }

     public function uploadBulkProduct( Request $request )
    {
              if($request->hasFile('product_file')){
                if (Input::file('product_file')->isValid()) {
                    $file = Input::file('product_file');
                    $destination = 'images/productFiles'.'/';
                    $ext= $file->getClientOriginalExtension();
                    $mainFilename = str_random(6).date('h-i-s');
                    $file->move($destination, $mainFilename.".".$ext);
                    
                    

                     $file = public_path($destination.$mainFilename.".".$ext);

                    $customerArr = $this->csvToArray($file);

                    for ($i = 0; $i < count($customerArr); $i ++)
                    {
                        // ::firstOrCreate($customerArr[$i]);
                                    $adminUser = DB::table($this->PRODUCT_TABLE)->insert([
                                                'product_name' => $customerArr[$i]['product_name'],
                                                'product_code' => $customerArr[$i]['product_code'],
                                                'product_tags' => $customerArr[$i]['product_tag'],
                                                'image'=>  $customerArr[$i]['path'],
                                                'product_description' => $customerArr[$i]['product_description'],
                                                'price_currency' => $customerArr[$i]['price_currency'],
                                                'product_old_price' => $customerArr[$i]['product_old_price'],
                                                'product_new_price' => $customerArr[$i]['product_new_price'],
                                                'product_share_url' => $customerArr[$i]['product_share_url'],
                                                'isactive' => $customerArr[$i]['is_active'],
                                                'category_id' => $customerArr[$i]['category_id']],
                                                );
                    }
                    return redirect('manage-product');
                }
            }
       
    }
    

function csvToArray($filename = '', $delimiter = ',')
{
    if (!file_exists($filename) || !is_readable($filename))
        return false;

    $header = null;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== false)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
        {
            if (!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }

    return $data;
}

    function csvToArrayProduct($filename = '', $delimiter = ',')
{
  
    $file = public_path('images/uploads/Mxb7pB05-11-36.csv');

    $customerArr = $this->csvToArray($file);

    for ($i = 0; $i < count($customerArr); $i ++)
    {
        // ::firstOrCreate($customerArr[$i]);
                     $adminUser = DB::table($this->PRODUCT_TABLE)->insert([
                                'product_name' => $customerArr[$i]['product_name'],
                                'product_code' => $customerArr[$i]['product_code'],
                                'product_tags' => $customerArr[$i]['product_tag'],
                                'image'=>  $customerArr[$i]['path'],
                                'product_description' => $customerArr[$i]['product_description'],
                                'price_currency' => $customerArr[$i]['price_currency'],
                                'product_old_price' => $customerArr[$i]['product_old_price'],
                                'product_new_price' => $customerArr[$i]['product_new_price'],
                                'product_share_url' => $customerArr[$i]['product_share_url'],
                                'isactive' => $customerArr[$i]['is_active'],
                                'category_id' => $customerArr[$i]['category_id']],
                                );
    }

}

    public function createProduct( Request $request )
    {
       
        return view('admin.product.create-product');
           
            
       
    }

    

    public function editCategoryInfo( Request $request )
    {
        if( $request->category_id )
        {

            $gh_category = DB::table($this->CATEGORY_TABLE)
            ->select('gh_category.*')
            ->where('gh_category.category_id', '=', $request->category_id)
            ->get();

            $locations = DB::table('locations')
            ->select('locations.location_id', 'locations.area')
            ->get();
            
            return view('admin.product.edit-category' , ['categoryList' => $gh_category[0] ,  'locations' => $locations]);
           }
        else
        {
            App::abort(404);
        }
    }

    
public function updateProductInfo( Request $request )
    {
        if($request->product_id)
        {
             $destination = null;
             if($request->hasFile('product_image')){
                if (Input::file('product_image')->isValid()) {
                    $file = Input::file('product_image');
                    $destination = 'images/uploads'.'/';
                    $ext= $file->getClientOriginalExtension();
                    $mainFilename = str_random(6).date('h-i-s');
                    $file->move($destination, $mainFilename.".".$ext);
                }
            }

            if($destination ==null){
                $gh_product= DB::table($this->PRODUCT_TABLE)
                ->where('product_id', $request->product_id)
                ->update(
                    [ 'product_name' => $request->product_name,
                                'product_code' => $request->product_code,
                                'weight' => $request->weight,
                                'product_description' => $request->product_description,
                                'product_price' => $request->product_price,
                                'bundle_size' => $request->bundle_size,
                                'quantity_in_bundle' => $request->quantity_in_bundle,
                                'isactive' => $request->is_active == "on" ? 1 : 0
                                    ]
                );
            }else{
                $gh_product= DB::table($this->PRODUCT_TABLE)
                ->where('product_id', $request->product_id)
                ->update(
                    [
                         'product_name' => $request->product_name,
                                'product_code' => $request->product_code,
                                'weight' => $request->weight,
                                'image'=> $destination.$mainFilename.".".$ext,
                                'product_description' => $request->product_description,
                                'product_price' => $request->product_price,
                                'bundle_size' => $request->bundle_size,
                                'quantity_in_bundle' => $request->quantity_in_bundle,
                                'isactive' => $request->is_active == "on" ? 1 : 0
                                    ]
                );
            }
           
            return redirect('manage-product/'.$request->product_id);
        }
        else
        {
            App::abort(404);
        }
       
    }



    public function updateCategoryInfo( Request $request )
    {
        if($request->category_id)
        {
       
           $updateResponse= DB::table($this->CATEGORY_TABLE)
            ->where('category_id', $request->category_id)
            ->update(
                ['category_name' => $request->category_name,
                'isactive' => $request->is_active == "on" ? 1 : 0,
                'location_id' => $request->location_id,
                ]
            );
            return redirect('manage-category/'.$request->category_id);
        }
        else
        {
            App::abort(404);
        }
       
    }
public function deleteProduct(Request $request)
    {
        if($request->product_id)
        {
            $deletedFlag = DB::table($this->PRODUCT_TABLE)->where('product_id', '=',$request->product_id )->delete();
            if($deletedFlag)
            {
                return redirect('manage-product'); 
            }
        }
        else
        {
            App::abort(404);
        }
    }

    public function deleteCategory(Request $request)
    {
        if($request->category_id)
        {
            $deletedFlag = DB::table($this->CATEGORY_TABLE)->where('category_id', '=',$request->category_id )->delete();
            if($deletedFlag)
            {
                return redirect('manage-category'); 
            }
        }
        else
        {
            App::abort(404);
        }
    }

    

    

}
