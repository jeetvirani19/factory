<?php

namespace App\Http\Controllers;
use App;
abstract class SchemaClass 
{
    const USER_TABLE = "users";
    const STORE_TABLE = "gh_store";
    const LOCATION_TABLE = "locations";
    const STORE_OFFERS_TABLE = "gh_store_offers";
    const STORE_OFFERS_PRODUCTS_TABLE = "gh_store_offers_products";
}
