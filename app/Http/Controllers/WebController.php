<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;
use App;


class WebController extends Controller
{
    //
    var $STORE_TABLE = "gh_store";
    var $USER_TABLE = "users";
    var $LOCATION_TABLE = "locations";
    var $STORE_OFFERS_TABLE = "gh_store_offers";
    var $STORE_OFFERS_PRODUCTS_TABLE = "gh_store_offers_products";
    var $PRODUCTS_TABLE = "gh_product";
    var $CATEGORY_TABLE = "gh_category";


    public function getStoreProduct(Request $request){
          $gh_product = DB::table($this->PRODUCTS_TABLE)
                 ->join($this->CATEGORY_TABLE, 'gh_category.category_id', '=', 'gh_product.category_id')
                 ->select('gh_product.*', 'gh_category.category_name')
                 ->where('gh_category.location_id','=',6)
                 ->get();
            $gh_category = DB::table($this->CATEGORY_TABLE)
                 ->select('gh_category.*')
                 ->where('gh_category.location_id','=',6)
                 ->get();
            return view('store.store-offer' , ['productList' => $gh_product,'categoryList'=>$gh_category]);
       
     }

    public function getStoreInfo( Request $request )
    {
        if( $request->id )
        {
            $gh_store = DB::table($this->STORE_TABLE)
            ->join($this->LOCATION_TABLE, $this->LOCATION_TABLE.'.location_id', '=', $this->STORE_TABLE.'.location_id')
            ->select($this->STORE_TABLE.'.*',$this->LOCATION_TABLE.'.area')
            ->where($this->STORE_TABLE.'.store_id', '=', $request->id)
            ->get();
            return view('admin.store.store-detail' , ['store' => $gh_store[0]]);
   
        }
        else
        {
            App::abort(404);
        }
    }

    public function createStore()
    {
        $locations = DB::table($this->LOCATION_TABLE)
        ->select($this->LOCATION_TABLE.'.location_id', $this->LOCATION_TABLE.'.area')
        ->get();
        return view('admin.store.create-store' , [$this->LOCATION_TABLE => $locations]);
    }

    public function saveStore( Request $request )
    {

        if($request->hasFile('store_image')){
                if (Input::file('store_image')->isValid()) {
                    $file = Input::file('store_image');
                    $destination = 'images/uploads'.'/';
                    $ext= $file->getClientOriginalExtension();
                    $mainFilename = str_random(6).date('h-i-s');
                    $file->move($destination, $mainFilename.".".$ext);
        $store_id = time()."";
        $newStore = DB::table($this->STORE_TABLE)->insert([
            'store_id' => $store_id,
            'image' => $destination.$mainFilename.".".$ext,
            'store_name' => $request->store_name,
            'store_address' => $request->store_address,
            'whatsapp_no' => $request->whatsapp_no,
            'fb_url' => $request->fb_url,
            'twitter_url' => $request->twitter_url,
            'location_id' => $request->location_id]);

            return redirect('manage-store/'.$store_id);
        }
    }

    }

    public function deleteStore(Request $request)
    {
        if($request->store_id)
        {
            $deletedFlag = DB::table($this->STORE_TABLE)->where('store_id', '=',$request->store_id )->delete();
            if($deletedFlag)
            {
                return redirect('manage-store/1/'.$request->store_name); 
            }
        }
        else
        {
            App::abort(404);
        }
    }

    public function editStoreInfo( Request $request )
    {
        if( $request->store_id )
        {
            $gh_store = DB::table($this->STORE_TABLE)
            ->join($this->LOCATION_TABLE, $this->LOCATION_TABLE.'.location_id', '=', $this->STORE_TABLE.'.location_id')
            ->select($this->STORE_TABLE.'.*',$this->LOCATION_TABLE.'.area')
            ->where($this->STORE_TABLE.'.store_id', '=', $request->store_id)
            ->get();

            $locations = DB::table($this->LOCATION_TABLE)
            ->select($this->LOCATION_TABLE.'.location_id', $this->LOCATION_TABLE.'.area')
            ->get();

            return view('admin.store.edit-store' , ['store' => $gh_store[0] , $this->LOCATION_TABLE => $locations]);
   
        }
        else
        {
            App::abort(404);
        }
    }

    public function updateStoreInfo( Request $request )
    {
        if($request->store_id)
        {
             $destination = null;
             if($request->hasFile('product_image')){
                if (Input::file('product_image')->isValid()) {
                    $file = Input::file('product_image');
                    $destination = 'images/uploads'.'/';
                    $ext= $file->getClientOriginalExtension();
                    $mainFilename = str_random(6).date('h-i-s');
                    $file->move($destination, $mainFilename.".".$ext);
                }
            }

            if($destination ==null){
                $updateResponse= DB::table($this->STORE_TABLE)
                ->where('store_id', $request->store_id)
                ->update(
                    ['store_name' => $request->store_name,
                    'location_id' => $request->location_id,
                    'whatsapp_no' => $request->whatsapp_no,
                    'fb_url' => $request->fb_url,
                    'twitter_url' => $request->twitter_url,
                    'store_address' => $request->store_address ],
                );
            }else{
                $updateResponse= DB::table($this->STORE_TABLE)
                ->where('store_id', $request->store_id)
                ->update(
                    ['store_name' => $request->store_name,
                    'location_id' => $request->location_id,
                    'image'=> $destination.$mainFilename.".".$ext,
                    'whatsapp_no' => $request->whatsapp_no,
                    'fb_url' => $request->fb_url,
                    'twitter_url' => $request->twitter_url,
                    'store_address' => $request->store_address ],
                );
            }

            return redirect('manage-store/'.$request->store_id);
        }
        else
        {
            App::abort(404);
        }
       
    }


    //--Store Offers
    public function fetchStoreOffers( Request $request )
    {
        if($request->storeId)
        {
            $storeQuery = DB::table($this->STORE_TABLE)->where($this->STORE_TABLE.'.store_id' , '=' , $request->storeId)->first();
            $storeOffersQuery = DB::table($this->STORE_OFFERS_TABLE)
            ->join($this->STORE_TABLE, $this->STORE_TABLE.'.store_id', '=', $this->STORE_OFFERS_TABLE.'.store_id')
            ->select($this->STORE_OFFERS_TABLE.'.*', $this->STORE_TABLE.'.store_name')
            ->where($this->STORE_OFFERS_TABLE.'.store_id' , '=' , $request->storeId)
            ->get();
            
            if(isset($request->deletedFlag)  && ($request->deletedFlag==1) && $request->deletedValue)
            {
                return view('admin.store-offers.store-offers-list' , ['storeOffersList' => $storeOffersQuery ,'storeQuery' => $storeQuery, 'deletedValue' => $request->deletedValue ,'storeId' => $request->storeId]);
            }
            else
            {
                return view('admin.store-offers.store-offers-list' , ['storeOffersList' => $storeOffersQuery ,'storeQuery' => $storeQuery, 'storeId' => $request->storeId]);
            }

        }
        else
        {
            App::abort(404);
        }
        
    }

    public function editStoreOfferInfo( Request $request )
    {
        if( $request->storeOfferId )
        {
            $storeOfferQuery = DB::table($this->STORE_OFFERS_TABLE)
            ->join($this->STORE_TABLE, $this->STORE_TABLE.'.store_id', '=', $this->STORE_OFFERS_TABLE.'.store_id')
            ->join($this->LOCATION_TABLE, $this->LOCATION_TABLE.'.location_id', '=', $this->STORE_TABLE.'.location_id')
            ->select($this->STORE_OFFERS_TABLE.'.*',$this->LOCATION_TABLE.'.area',$this->STORE_TABLE.'.store_name',$this->STORE_TABLE.'.location_id')
            ->where($this->STORE_OFFERS_TABLE.'.offer_id', '=', $request->storeOfferId)
            ->get();

            if( count($storeOfferQuery) > 0 )
            {
                $storeOfferProductsQuery = DB::table($this->STORE_OFFERS_PRODUCTS_TABLE)
                ->join($this->PRODUCTS_TABLE , $this->PRODUCTS_TABLE.'.product_id','=',$this->STORE_OFFERS_PRODUCTS_TABLE.'.product_id')
                ->select($this->STORE_OFFERS_PRODUCTS_TABLE.'.*' , $this->PRODUCTS_TABLE.'.product_name')
                ->where($this->STORE_OFFERS_PRODUCTS_TABLE.'.store_offer_id','=',$request->storeOfferId)
                ->get();
    
                // if(Auth::user()->user_type == 0)
                // {
                //     $productsQuery =  DB::table($this->PRODUCTS_TABLE)
                //     ->join($this->CATEGORY_TABLE, function ($join) {
                //         $join->on($this->PRODUCTS_TABLE.'.category_id', '=', $this->CATEGORY_TABLE.'.category_id');
                //     })
                //     ->get();
                // }
                // else
                // {
                    
                    $this->loc = $storeOfferQuery[0]->location_id;
                    
                    $productsQuery =  DB::table($this->PRODUCTS_TABLE)
                    ->join($this->CATEGORY_TABLE, function ($join) {
                        $join->on($this->PRODUCTS_TABLE.'.category_id', '=', $this->CATEGORY_TABLE.'.category_id')
                        ->where($this->CATEGORY_TABLE.'.location_id', '=', $this->loc);
                    })
                    ->get();
                // }
                
    
                $prodArr = array();
                for ($i=0; $i < count($storeOfferProductsQuery); $i++) { 
                    $offerProd = $storeOfferProductsQuery[$i];
                    array_push($prodArr ,  
                        [
                            "old_product_price"=> $offerProd->old_product_price,
                            "new_product_price"=> $offerProd->new_product_price,
                            "product_id"=> $offerProd->product_id,
                            "product_name"=> $offerProd->product_name
                        ]);
                }
                
                $storeOfferProductsJSON = array("products" => $prodArr);
                return view('admin.store-offers.edit-store-offer' , ['storeOffer' => $storeOfferQuery[0],'productsList'=>$productsQuery ,'storeOfferProductsJSON' => json_encode($storeOfferProductsJSON)]);
   
            }
            else
            {
                App::abort(404);
            }
           
        }
        else
        {
            App::abort(404);
        }
    }

    public function updateStoreOfferInfo(Request $request)
    {
        if( $request->storeOfferId )
        {
            $updateStoreOffer = DB::table($this->STORE_OFFERS_TABLE)
            ->where('offer_id', $request->storeOfferId)
            ->update([
            'store_offer_name' => $request->store_name,
            'is_published' => $request->is_published == "on" ? 1 : 0,
            'offer_start_date' => date( 'Y-m-d', strtotime( $request->offer_start_date_dp ) ),
            'offer_end_date' =>  date( 'Y-m-d', strtotime( $request->offer_end_date_dp ) ) ]);

            if($request->store_products)
            {
                $deleteQuery = DB::table($this->STORE_OFFERS_PRODUCTS_TABLE)->where('store_offer_id', '=', $request->storeOfferId)->delete();
                if($deleteQuery)
                {
                    $store_products_json = json_decode($request->store_products);
                    $insertArr = array();
                    if(count($store_products_json->products) > 0)
                    {
                        $index_count = 0;
                        foreach ($store_products_json->products as $prod) {
                            $index_count++;
                            array_push($insertArr ,[
                                'product_id' => $prod->product_id,
                                'new_product_price' => $prod->new_product_price,
                                'old_product_price' => $prod->old_product_price,
                                'store_offer_id' => $request->storeOfferId,
                                'store_offers_products_id' => time().$index_count.""
                            ]);
                        }
                        $newStoreOfferProducts = DB::table($this->STORE_OFFERS_PRODUCTS_TABLE)->insert($insertArr);
                        if($newStoreOfferProducts)
                        {
                            return redirect('view-store-offer-detail/'.$request->storeOfferId);
                        }
                        else
                        {
                            return redirect('view-store-offer-detail/'.$request->storeOfferId);
                        }
                    }
                    else
                    {
                        App::abort(404);
                    }
                }
                else
                {
                    echo "Error deleting Store Offer Products";
                }
                
            }  
           
        }
        else
        {
            App::abort(404);
        }
        
    }

    public function getStoreOfferDetail(Request $request)
    {
        if($request->storeOfferId)
        {
           
            $storeOffersQuery = DB::table($this->STORE_OFFERS_TABLE)
            ->select($this->STORE_OFFERS_TABLE.'.*')
            ->where($this->STORE_OFFERS_TABLE.'.offer_id' , '=' , $request->storeOfferId)
            ->get();

            $storeOfferProductsQuery = DB::table($this->STORE_OFFERS_PRODUCTS_TABLE)
            ->join($this->PRODUCTS_TABLE, $this->PRODUCTS_TABLE.'.product_id', '=', $this->STORE_OFFERS_PRODUCTS_TABLE.'.product_id')
            ->select($this->STORE_OFFERS_PRODUCTS_TABLE.'.*',$this->PRODUCTS_TABLE.'.product_id',$this->PRODUCTS_TABLE.'.product_name',$this->PRODUCTS_TABLE.'.image' )
            ->where($this->STORE_OFFERS_PRODUCTS_TABLE.'.store_offer_id' , '=' , $request->storeOfferId)
            ->get();
  
             return view('admin.store-offers.store-offer-detail' , ['storeOffersQuery' => $storeOffersQuery[0] , 'storeOfferProductsQuery' => $storeOfferProductsQuery]);
        }
        else
        {
            App::abort(404);
        }
    }

    public function createStoreOfferForm( Request $request )
    {
        if($request->storeId)
        {
            // if(Auth::user()->user_type == 0)
            // {
            //     $productsQuery =  DB::table($this->PRODUCTS_TABLE)
            //     ->join($this->CATEGORY_TABLE, function ($join) {
            //         $join->on($this->PRODUCTS_TABLE.'.category_id', '=', $this->CATEGORY_TABLE.'.category_id');
            //     })
            //     ->get();
            // }
            // else
            // {
                
                $storeQuery = DB::table($this->STORE_TABLE)
                ->select($this->STORE_TABLE.'.*')
                ->where($this->STORE_TABLE.'.store_id', '=', $request->storeId)
                ->get();

                if( count($storeQuery) > 0 )
                {
                    $this->loc = $storeQuery[0]->location_id;
                    $productsQuery =  DB::table($this->PRODUCTS_TABLE)
                    ->join($this->CATEGORY_TABLE, function ($join) {
                        $join->on($this->PRODUCTS_TABLE.'.category_id', '=', $this->CATEGORY_TABLE.'.category_id')
                        ->where($this->CATEGORY_TABLE.'.location_id', '=', $this->loc);
                    })
                    ->get();
                    return view('admin.store-offers.create-store-offer' , ['storeId' => $request->storeId , 'productsList' => $productsQuery]);
                }
                else
                {
                    App::abort(404);
                }
            // }
            
        }
        else
        {
            App::abort(404);
        }
    }

    public function saveStoreOffer( Request $request )
    {
        $store_offer_id = time()."";
        $newStoreOffer = DB::table($this->STORE_OFFERS_TABLE)->insert([
            'offer_id' => $store_offer_id,
            'store_offer_name' => $request->store_name,
            'store_id' => $request->store_id,
            'is_published' => $request->is_published == "on" ? 1 : 0,
            'offer_start_date' => date( 'Y-m-d', strtotime( $request->offer_start_date_dp ) ),
            'offer_end_date' =>  date( 'Y-m-d', strtotime( $request->offer_end_date_dp ) ) ]);
        if($request->store_products)
        {
            $store_products_json = json_decode($request->store_products);
            $insertArr = array();
            if(count($store_products_json->products) > 0)
            {
                $index_count = 0;
                foreach ($store_products_json->products as $prod) {
                    $index_count++;
                    array_push($insertArr ,[
                        'product_id' => $prod->product_id,
                        'new_product_price' => $prod->new_product_price,
                        'old_product_price' => $prod->old_product_price,
                        'store_offer_id' => $store_offer_id,
                        'store_offers_products_id' => time().$index_count.""
                    ]);
                    
                }
                $newStoreOfferProducts = DB::table($this->STORE_OFFERS_PRODUCTS_TABLE)->insert($insertArr);
                if($newStoreOfferProducts)
                {
                    return redirect('view-store-offer-detail/'.$store_offer_id);
                }
            }
        }    
    }
    public function deleteStoreOffer(Request $request)
    {
        if($request->store_offer_id)
        {
            $deletedFlag = DB::table($this->STORE_OFFERS_TABLE)->where('offer_id', '=',$request->store_offer_id )->delete();
            if($deletedFlag)
            {
                return redirect('view-store-offers/'.$request->store_id.'/1/'.$request->store_offer_name); 
            }
        }
        else
        {
            App::abort(404);
        }
    }
}
