<?php 
$cssurl = url('css')."/";
$jsurl = url('js')."/";
$imgurl = url('images')."/";
$siteurl = url('/');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon.png">
  <link rel="icon" type="image/png" href="../img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
      @yield('pagetitle')
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="<?php echo $cssurl;?>bootstrap.min.css" rel="stylesheet" />
  <link href="<?php echo $cssurl;?>paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <link href="<?php echo $cssurl;?>ghcss.scss" rel="stylesheet" />
  
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="white" data-active-color="danger">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
      <div class="logo">
        <!-- <a href="http://www.creative-tim.com" class="simple-text logo-mini">
          <div class="logo-image-small">
            <img src="../img/logo-small.png">
          </div>
        </a> -->
        <a href="<?php echo $siteurl;?>/" class="simple-text logo-normal">
          &nbsp;Factory Management
          <!-- <div class="logo-image-big">
            <img src="../img/logo-big.png">
          </div> -->
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li id="dashboard">
            <a href="<?php echo $siteurl;?>/dashboard">
              <i class="nc-icon nc-bank"></i>
              <p>Dashboard</p>
            </a>
          </li> 
          <?php if(Auth::user()->user_type == 0){?>
          <li id="admins">
            <a href="<?php echo $siteurl;?>/manage-admin">
              <i class="nc-icon nc-single-02"></i>
              <p>Manage Admins</p>
            </a>
          </li>
          <?php } ?>
          <li id="products">
                <a href="<?php echo $siteurl;?>/manage-product">
                  <i class="nc-icon nc-app"></i>
                  <p>Manage Products</p>
                </a>
              </li>
              <li id="invoices">
                <a href="<?php echo $siteurl;?>/manage-invoice">
                  <i class="nc-icon nc-app"></i>
                  <p>Manage sales</p>
                </a>
              </li>
              <li id="materials">
                <a href="<?php echo $siteurl;?>/manage-material">
                  <i class="nc-icon nc-app"></i>
                  <p>Manage Materials</p>
                </a>
              </li>
              <li id="machines">
                <a href="<?php echo $siteurl;?>/manage-machine">
                  <i class="nc-icon nc-app"></i>
                  <p>Machine Settings</p>
                </a>
              </li>
        
              
              
              <?php if(Auth::user()->user_type == 0){?>
              <!-- <li id="careers">
                  <a href="./user.html">
                  <i class="nc-icon nc-circle-10"></i>
                  <p>Career Information</p>
                </a>
              </li> <?php }?>
              <?php if(Auth::user()->user_type == 0){?>
              <li id="events">
                <a href="./user.html">
                  <i class="nc-icon nc-paper"></i>
                  <p>Manage News/Events</p>
                </a>
              </li>
              <li id="queries"> <?php }?>
              <?php if(Auth::user()->user_type == 0){?><a href="./user.html">
                  <i class="nc-icon nc-zoom-split"></i>
                  <p>Customer Queries</p>
                </a>
              </li> -->
              <li> <?php }?>
              
             
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            @yield('page-header-name')
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
            <ul class="navbar-nav">
              <li class="nav-item btn-rotate dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="nc-icon nc-single-02"></i>&nbsp;{{ Auth::user()->name }}
                  <p>
                    <span class="d-lg-none d-md-block">My Acount</span>
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <!-- <a class="dropdown-item" href="#">View Profile</a> -->
                  <a class="dropdown-item" href="<?php echo $siteurl;?>/logout">Logout</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <!-- <div class="panel-header panel-header-lg">
  
  <canvas id="bigDashboardChart"></canvas>
  
  
</div> -->
    @yield('main-content')
      <footer class="footer footer-black  footer-white ">
        <div class="container-fluid">
          <div class="row">
            <div class="credits ml-auto">
              <span class="copyright">
                ©
                <script>
                  document.write(new Date().getFullYear())
                </script> Factory Management
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo $jsurl;?>core/jquery.min.js"></script>
  <script src="<?php echo $jsurl;?>core/popper.min.js"></script>
  <script src="<?php echo $jsurl;?>core/bootstrap.min.js"></script>
  
  <script src="<?php echo $jsurl;?>plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo $jsurl;?>plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo $jsurl;?>paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
  <script>
  function showNotification( from , align , noti_message )
    {
        color = 'danger';
        $.notify({
            icon: "nc-icon nc-bell-55",
            message: noti_message

        },{
            type: color,
            timer: 8000,
            placement: {
                from: from,
                align: align
            }
        });
    }</script>
  @yield('extended-files')
  
 </body>

</html>