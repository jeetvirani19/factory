@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='#'>Create invoice</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create invoice</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="./save-invoice" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Customer Name</label>
                <input type="text" class="form-control" name="customer_name" required placeholder="customer Name" >
                </div>
            </div>
                 <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Name</label>
                <?php
                    echo "<select name='product_id' class='form-control'>";
                    for($i = 0; $i < count($productList); $i++)
                    {                       
                            echo '<option value="'.$productList[$i]->product_id.'">'.$productList[$i]->product_name.'</option>';                                                   
                    }
                    echo "</select>";
                ?>    
                </div>
        
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Quantity</label>
                <input type="text" class="form-control" name="product_qty" id="product_qty" required placeholder="product quantity" >
                </div>
            </div>
          
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Price</label>
                <input type="text" class="form-control" name="product_price" id="product_price" required placeholder="product price" >
                </div>
            </div>
             
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Total Amount</label>
                <input type="text" class="form-control" name="total" id="total" required placeholder="total" >
                </div>
            </div>
           
            </div>
          
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
                <a href='<?php echo url("/manage-invoice");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script>
   
    $("#product_qty").change(function(){
        let qty = $("#product_qty").val();
        let price = $("#product_price").val();
        document.getElementById("total").value = Number(qty)*Number(price);
    });
    $("#product_price").change(function(){
        let qty = $("#product_qty").val();
        let price = $("#product_price").val();
        document.getElementById("total").value = Number(qty)*Number(price);
    });
    
    $(document).ready(function() {
      $('#invoices').addClass("active");
    });
</script>

@stop