@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-category");?>'>Manage invoice</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$invoiceList->customer_name}}</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Edit invoice</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../update-invoice/{{$invoiceList->invoice_id}}" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Customer Name</label>
                <input type="text" class="form-control" name="customer_name" required placeholder="customer Name" value='{{$invoiceList->customer_name}}'>
                </div>
            </div>
         
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Quantity</label>
                <input type="text" class="form-control" name="product_qty" id="product_qty" required placeholder="product quantity" value='{{$invoiceList->product_qty}}' >
                </div>
            </div>
          
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Price</label>
                <input type="text" class="form-control" name="product_price" id="product_price" required placeholder="product price" value='{{$invoiceList->product_price}}' >
                </div>
            </div>

             
             
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Total Amount</label>
                <input type="text" class="form-control" name="total" id="total" required placeholder="total" value='{{$invoiceList->total}}' >
                </div>
            </div>
            </div>
            <div class="row">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Update</button>
                 <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$invoiceList->invoice_id','$invoiceList->customer_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                 <a href='<?php echo url("/manage-invoice");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>

         <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete invoice</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteinvoice()" class="btn btn-danger" data-dismiss="modal">Delete invoice</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var invoiceId ="", invoicename="";
    $(document).ready(function() {
      $('#images').addClass("active");
    });
    var invoiceId ="", invoicename="";
    function confirmDelete(invoice_id , recordName)
    {
        invoiceId = invoice_id;
        invoicename = recordName;
    }

    function deleteinvoice()
    {
        if(invoiceId != "" && invoicename !="")
        {
            redurl = "/delete-invoice/"+invoiceId+"/"+invoicename;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop