@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-product");?>'>Manage invoice</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$invoiceList->customer_name}}</a>
@stop
@section('main-content')
  <div class="content">
    <!-- <div class="container-fluid">
        <h1 class="navbar-brand" href="#">Welcome User!</h1>
    </div> -->
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header">
            <h5 class="card-title">View invoice</h5>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Customer Name</label>
                    <label class="form-control">{{$invoiceList->customer_name}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Product Name</label>
                    <label class="form-control">{{$invoiceList->product_name}} </label>
                  </div>
                </div>
                
              </div>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Product Price</label>
                    <label class="form-control">{{$invoiceList->product_price}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Quantiry</label>
                    <label class="form-control">{{$invoiceList->product_qty}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Total</label>
                    <label class="form-control">{{$invoiceList->total}}</label>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-12">
                     <a href='<?php echo url("/edit-invoice/{$invoiceList->invoice_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>               
                      <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$invoiceList->invoice_id','$invoiceList->customer_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                      <a href='<?php echo url("/manage-invoice");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
  </div>
  
         <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete invoice</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteinvoice()" class="btn btn-danger" data-dismiss="modal">Delete invoice</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var invoiceId ="", invoicename="";
    $(document).ready(function() {
      $('#images').addClass("active");
    });
    var invoiceId ="", invoicename="";
    function confirmDelete(invoice_id , recordName)
    {
        invoiceId = invoice_id;
        invoicename = recordName;
    }

    function deleteinvoice()
    {
        if(invoiceId != "" && invoicename !="")
        {
            redurl = "/delete-invoice/"+invoiceId+"/"+invoicename;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop