@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-invoice");?>'>Manage invoice</a>
@stop
@section('main-content')
    <div class="content">
      <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-flex">
                    <h4 class="card-title"> Invoices</h4>
                    <a href='<?php echo url("/create-invoice");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Create invoice</a> 
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                            <thead class=" text-primary">
                                <th>
                                Customer Name
                                </th>
                                <th class="text-center">
                                Total
                                </th>
                                <th class="text-center">
                                Order No
                                </th>
                                <th class="text-center">
                                Actions
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $invoiceList as $invoice )
                                <tr>
                                <td>
                                    {{$invoice->customer_name}} 
                                </td>
                                <td class="text-center">
                                    {{ $invoice->total }}
                                </td>
                                <td class="text-center">
                                    {{ $invoice->product_id }}
                                </td>
                                <td class="text-center">
                                    <a href='<?php echo url("/manage-invoice/{$invoice->invoice_id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-glasses-2"></i>&nbsp;View</a>
                                    <a href='<?php echo url("/edit-invoice/{$invoice->invoice_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                     
                                   <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$invoice->invoice_id','$invoice->customer_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete invoice</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteinvoice()" class="btn btn-danger" data-dismiss="modal">Delete invoice</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var invoiceId ="", invoicename="";
    $(document).ready(function() {
      $('#images').addClass("active");
    });
    var invoiceId ="", invoicename="";
    function confirmDelete(invoice_id , recordName)
    {
        invoiceId = invoice_id;
        invoicename = recordName;
    }

    function deleteinvoice()
    {
        if(invoiceId != "" && invoicename !="")
        {
            redurl = "/delete-invoice/"+invoiceId+"/"+invoicename;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop