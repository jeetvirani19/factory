@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='#'>Create machine</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create machine</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="./save-machine" enctype="multipart/form-data">
            <div class="row">
              
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Machine Name</label>
                <input type="text" class="form-control" name="machine_name" required placeholder="Machine
                 Name" >
                </div>
            </div>
                 <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Temperature</label>
                <input type="text" class="form-control" name="temperature" id="temperature" required placeholder="temperature" >
                </div>
        
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Locking</label>
                <input type="text" class="form-control" name="locking" id="locking" required placeholder="Locking" >
                </div>
            </div>
          
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Pressure</label>
                <input type="text" class="form-control" name="pressure" id="pressure" required placeholder="pressure" >
                </div>
            </div>
             
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Scale</label>
                <input type="text" class="form-control" name="scale" id="scale" required placeholder="scale" >
                </div>
            </div>
           
            </div>
          
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
                <a href='<?php echo url("/manage-machine");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script>
   
    $("#product_qty").change(function(){
        let qty = $("#product_qty").val();
        let price = $("#product_price").val();
        document.getElementById("total").value = Number(qty)*Number(price);
    });
    $("#product_price").change(function(){
        let qty = $("#product_qty").val();
        let price = $("#product_price").val();
        document.getElementById("total").value = Number(qty)*Number(price);
    });
    
    $(document).ready(function() {
      $('#machines').addClass("active");
    });
</script>

@stop