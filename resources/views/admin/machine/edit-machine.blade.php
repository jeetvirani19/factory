@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-category");?>'>Manage machine</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$machineList->machine_name}}</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Edit machine</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../update-machine/{{$machineList->machine_id}}" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Machine Name</label>
                <input type="text" class="form-control" name="machine_name" required placeholder="machine Name" value='{{$machineList->machine_name}}'>
                </div>
            </div>
         
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Temperature</label>
                <input type="text" class="form-control" name="temperature" id="temperature" required placeholder="product quantity" value='{{$machineList->temperature}}' >
                </div>
            </div>
          
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Scale</label>
                <input type="text" class="form-control" name="scale" id="scale" required placeholder="scale" value='{{$machineList->scale}}' >
                </div>
            </div>

             
             
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>locking</label>
                <input type="text" class="form-control" name="locking" id="locking" required placeholder="locking" value='{{$machineList->locking}}' >
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>pressure</label>
                <input type="text" class="form-control" name="pressure" id="pressure" required placeholder="pressure" value='{{$machineList->locking}}' >
                </div>
            </div>
            </div>
            <div class="row">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Update</button>
                 <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$machineList->machine_id','$machineList->machine_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                 <a href='<?php echo url("/manage-machine");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>

         <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete machine</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deletemachine()" class="btn btn-danger" data-dismiss="modal">Delete machine</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var machineId ="", machinename="";
    $(document).ready(function() {
      $('#images').addClass("active");
    });
    var machineId ="", machinename="";
    function confirmDelete(machine_id , recordName)
    {
        machineId = machine_id;
        machinename = recordName;
    }

    function deletemachine()
    {
        if(machineId != "" && machinename !="")
        {
            redurl = "/delete-machine/"+machineId+"/"+machinename;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop