@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-product");?>'>Manage machine</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$machineList->machine_name}}</a>
@stop
@section('main-content')
  <div class="content">
    <!-- <div class="container-fluid">
        <h1 class="navbar-brand" href="#">Welcome User!</h1>
    </div> -->
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header">
            <h5 class="card-title">View machine</h5>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Machine Name</label>
                    <label class="form-control">{{$machineList->machine_name}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Temperature</label>
                    <label class="form-control">{{$machineList->temperature}} </label>
                  </div>
                </div>
                
              </div>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Locking</label>
                    <label class="form-control">{{$machineList->locking}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Pressure</label>
                    <label class="form-control">{{$machineList->pressure}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Scale</label>
                    <label class="form-control">{{$machineList->scale}}</label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Modified Date</label>
                    <label class="form-control"><?php echo  date("F j, Y, g:i A", strtotime($machineList->modified_date));?></label>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-12">
                     <a href='<?php echo url("/edit-machine/{$machineList->machine_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>               
                      <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$machineList->machine_id','$machineList->machine_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                      <a href='<?php echo url("/manage-machine");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
  </div>
  
         <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete machine</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deletemachine()" class="btn btn-danger" data-dismiss="modal">Delete machine</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var machineId ="", machinename="";
    $(document).ready(function() {
      $('#images').addClass("active");
    });
    var machineId ="", machinename="";
    function confirmDelete(machine_id , recordName)
    {
        machineId = machine_id;
        machinename = recordName;
    }

    function deletemachine()
    {
        if(machineId != "" && machinename !="")
        {
            redurl = "/delete-machine/"+machineId+"/"+machinename;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop