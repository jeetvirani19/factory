@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-machine");?>'>Manage machine</a>
@stop
@section('main-content')
    <div class="content">
      <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-flex">
                    <h4 class="card-title"> machines</h4>
                    <a href='<?php echo url("/create-machine");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Create machine</a> 
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                            <thead class=" text-primary">
                                <th>
                                machine Name
                                </th>
                                <th class="text-center">
                                temperature
                                </th>
                                <th class="text-center">
                                locking
                                </th>
                                <th class="text-center">
                                scale
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $machineList as $machine )
                                <tr>
                                <td>
                                    {{$machine->machine_name}} 
                                </td>
                                <td class="text-center">
                                    {{ $machine->temperature }}
                                </td>
                                <td class="text-center">
                                    {{ $machine->locking }}
                                </td>
                                <td class="text-center">
                                    {{ $machine->scale }}
                                </td>
                                <td class="text-center">
                                    <a href='<?php echo url("/manage-machine/{$machine->machine_id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-glasses-2"></i>&nbsp;View</a>
                                    <a href='<?php echo url("/edit-machine/{$machine->machine_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                     
                                   <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$machine->machine_id','$machine->machine_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete machine</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deletemachine()" class="btn btn-danger" data-dismiss="modal">Delete machine</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var machineId ="", machinename="";
    $(document).ready(function() {
      $('#images').addClass("active");
    });
    var machineId ="", machinename="";
    function confirmDelete(machine_id , recordName)
    {
        machineId = machine_id;
        machinename = recordName;
    }

    function deletemachine()
    {
        if(machineId != "" && machinename !="")
        {
            redurl = "/delete-machine/"+machineId+"/"+machinename;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop