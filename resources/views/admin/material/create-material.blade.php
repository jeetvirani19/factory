@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href="#pablo">Create Material</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create materail</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../save-material" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-5 px-1">
                <div class="form-group">
                <label>name</label>
                <input type="text" class="form-control" name="name" required placeholder="name">
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>seller</label>
                <input type="text" class="form-control" name="seller" required placeholder="seller" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>quantity</label>
                <input type="text" class="form-control" name="quantity" required placeholder="quantity" >
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>price</label>
                <input type="text" class="form-control" name="price" required placeholder="price" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>quality</label>
                <input type="text" class="form-control" name="quality" required placeholder="quality" >
                </div>
            </div>
             <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Purchase Date</label>
                <input type="text" class="form-control" name="dateOfPurchase" id="dateOfPurchase" required placeholder="Purchase Date" >
                </div>
            </div>
            </div>
           
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
                <a href='<?php echo url("/manage-material");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>

    $(document).ready(function() {
      $('#materials').addClass("active");
       $('#dateOfPurchase').datepicker({
           uiLibrary: 'bootstrap4'
      });
      $('#offer_end_date_dp').datepicker({
           uiLibrary: 'bootstrap4'
      });
    });
      
</script>

@stop