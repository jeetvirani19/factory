@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href="#pablo">Edit Material</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Edit Material</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../update-material/{{$materialList->material_id}}" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>name</label>
                <input type="text" class="form-control" name="name" required placeholder="name" value='{{$materialList->name}}'>
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Seller</label>
                <input type="text" class="form-control" name="seller" required placeholder="seller" value='{{$materialList->seller}}'>
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Quantity</label>
                <input type="text" class="form-control" name="quantity" required placeholder="quantity" value='{{$materialList->quantity}}'>
                </div>
            </div>
         
            </div>
            <div class="row">

            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Quality</label>
                <input type="text" class="form-control" name="quality" required placeholder="quality" value='{{$materialList->quality}}'>
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Price</label>
                <input type="text" class="form-control" name="price" required placeholder="price" value='{{$materialList->price}}'>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Update</button>
                 <a href='<?php echo url("/manage-material");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#materials').addClass("active");
    });
</script>

@stop