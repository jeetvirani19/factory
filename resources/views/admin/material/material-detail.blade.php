@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href="#pablo">material Detail</a>
@stop
@section('main-content')
  <div class="content">
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header">
            <h5 class="card-title">View material</h5>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>name</label>
                    <label class="form-control">{{$materialList->name}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>seller</label>
                    <label class="form-control">{{$materialList->seller}} </label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">quantity</label>
                   <label class="form-control">{{$materialList->quantity}} </label>
                  </div>
                </div>
                 <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">price</label>
                   <label class="form-control">{{$materialList->price}} </label>
                  </div>
                </div>
                 <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">quality</label>
                   <label class="form-control">{{$materialList->quality}} </label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">purchase_date</label>
                   <label class="form-control">{{$materialList->purchase_date}} </label>
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-12">
                  <a href='<?php echo url("/edit-material/{$materialList->material_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                  <a href='<?php echo url("/delete-material/{$materialList->material_id}/{$materialList->name}");?>' class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                  <a href='<?php echo url("/manage-material");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
  </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#materials').addClass("active");
    });
</script>

@stop