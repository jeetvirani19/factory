@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-category");?>'>Manage Category</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$categoryList->category_name}}</a>
@stop
@section('main-content')
  <div class="content">
    <!-- <div class="container-fluid">
        <h1 class="navbar-brand" href="#">Welcome User!</h1>
    </div> -->
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header">
            <h5 class="card-title">View Category - {{$categoryList->category_name}}</h5>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Name</label>
                    <label class="form-control">{{$categoryList->category_name}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Area</label>
                    <label class="form-control">{{$location->area}} - {{$location->country}} </label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                <div class="form-group">
                    <label>Active</label>
                    <label class="form-control">
                      <?php 
                      if($categoryList->isactive) { echo "Yes";}
                      else {echo "No";}
                      ?>
                    </label>
                  </div>
                </div>
               
              </div>
              
              <div class="row">
                <div class="col-md-12">
                 <a href='<?php echo url("/edit-category/{$categoryList->category_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
    
                    <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$categoryList->category_id','$categoryList->category_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                        <a href='<?php echo url("/manage-category");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
  </div>
 <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteCategory()" class="btn btn-danger" data-dismiss="modal">Delete Category</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#categories').addClass("active");
    });
    var categoryId ="", categoryname="";
  function confirmDelete(category_id , recordName)
    {
        categoryId = category_id;
        categoryname = recordName;
    }

    function deleteCategory()
    {
        if(categoryId != "" && categoryname !="")
        {
            redurl = "/delete-category/"+categoryId+"/"+categoryname;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop