@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-category");?>'>Manage Category</a>
@stop
@section('main-content')
    <div class="content">
      <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-flex">
                    <h4 class="card-title"> Categories</h4>
                    <a href='<?php echo url("/create-category");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Create Category</a> 
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                            <thead class=" text-primary">
                                <th>
                                Name
                                </th>
                                <th class="text-center">
                                Active
                                </th>
                                <th class="text-center">
                                Area
                                </th>
                                <th class="text-center">
                                Actions
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $categoryList as $category )
                                <tr>
                                <td>
                                    {{$category->category_name}} 
                                </td>
                                <td class="text-center">
                                <?php if($category->isactive)
                                      {echo '<i class="is-active-check nc-icon nc-check-2"></i>';}
                                      else
                                      {echo '<i class="is-active-check nc-icon nc-simple-remove"></i>';}
                                  ?>
                                </td>
                                <td class="text-center">
                                  {{$category->area}}
                                </td>
                                <td class="text-center">
                                    <a href='<?php echo url("/manage-category/{$category->category_id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-glasses-2"></i>&nbsp;View</a>
                                    <a href='<?php echo url("/edit-category/{$category->category_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                   <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$category->category_id','$category->category_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
       <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteCategory()" class="btn btn-danger" data-dismiss="modal">Delete Category</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#categories').addClass("active");
    });
    var categoryId ="", categoryname="";
  function confirmDelete(category_id , recordName)
    {
        categoryId = category_id;
        categoryname = recordName;
    }

    function deleteCategory()
    {
        if(categoryId != "" && categoryname !="")
        {
            redurl = "/delete-category/"+categoryId+"/"+categoryname;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop