@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='#'>Create Category</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create Category</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="./save-category" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Category Name</label>
                <input type="text" class="form-control" name="category_name" required placeholder="category Name" >
                </div>
            </div>
            </div>
            <div class="row">
           <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Area</label>
                 <?php
                    echo "<select name='location_id' class='form-control'>";
                    for($i = 0; $i < count($locations); $i++)
                    {
                        if( Auth::user()->user_type == 1 )
                        {
                            if(Auth::user()->location_id == $locations[$i]->location_id)
                            {
                                echo '<option value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';   
                            }
                        }
                        else
                        {
                            echo '<option value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';   
                        }
                            
                    }
                    echo "</select>";
                ?>    
                </div>
                </div>
        
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <!-- <label>Active</label><br/>
                <input type="checkbox"  name="is_active" value=""> -->
                <br/>
                <div class="custom-control custom-checkbox">
                  
                        <input type='checkbox' name='is_active' class='custom-control-input' id='customCheck1'>
                   
                <label class="custom-control-label" for="customCheck1">Active</label>
                </div>
                </div>
            </div>
            </div>
           
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
               <a href='<?php echo url("/manage-category");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#categories').addClass("active");
    });
</script>

@stop