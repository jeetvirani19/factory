@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='#'>Create Product</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create Product</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../save-product" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Name</label>
                <input type="text" class="form-control" name="product_name" required placeholder="Product Name" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Product code</label>
                <input type="text" class="form-control" name="product_code" required placeholder="Product code" >
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product description</label>
                <input type="text" class="form-control" name="product_description" required placeholder="Product description" >
                </div>
            </div>
             <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Price</label>
                <input type="text" class="form-control" name="product_price" required placeholder="Product price" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <!-- <label>Active</label><br/>
                <input type="checkbox"  name="is_active" value=""> -->
                <br/>
                <div class="custom-control custom-checkbox">
                  
                        <input type='checkbox' name='is_active' class='custom-control-input' id='customCheck1'>
                   
                <label class="custom-control-label" for="customCheck1">Active</label>
                </div>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Bundle size</label>
                <input type="text" class="form-control" name="bundle_size" required placeholder="bundle size" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Quantity</label>
                <input type="text" class="form-control" name="quantity_in_bundle" required placeholder="quantity in bundle" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>weight </label>
                <input type="text" class="form-control" name="weight" required placeholder="weight" >
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-offset-1 col-md-5 pl-1">
                <label>Product Image</label>
             
                  <input type="file" name="product_image" class="form-control" id="product_image"  >
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
                <button class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Cancel</button>
            </div>
            </div>
        </form>
        <div>
                    <h3>
                    OR
                    </h3>
        </div>
        <form method="POST" action="../upload-product" enctype="multipart/form-data">
               <div class="row">
            <div class="col-md-12">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="card-header card-header-flex col-md-3"><label>Upload CSV(product)</label>
                  <input type="file" name="product_file" class="form-control" id="product_file" required>
                   <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Upload</button>
                  </div>
            </div>
            </div>      
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#products').addClass("active");
    });
</script>

@stop