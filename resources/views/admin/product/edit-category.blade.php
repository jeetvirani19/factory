@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-category");?>'>Manage Category</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$categoryList->category_name}}</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Edit Category - {{$categoryList->category_name}}</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../update-category/{{$categoryList->category_id}}" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-5 px-1">
                  <div class="form-group">
                  <label>Category Name</label>
                  <input type="text" class="form-control" name="category_name" required placeholder="category Name" value='{{$categoryList->category_name}}'>
                  </div>
              </div>
              <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Area</label>
                    <?php
                        echo "<select name='location_id' class='form-control'>";
                        for($i = 0; $i < count($locations); $i++)
                        {
                            if( Auth::user()->user_type == 1 )
                            {
                                if(Auth::user()->location_id == $locations[$i]->location_id)
                                {
                                    echo '<option value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';   
                                }
                            }
                            else
                            {
                                if($categoryList->location_id == $locations[$i]->location_id)
                                {
                                    echo '<option selected value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';   
                                }
                                else
                                {
                                  echo '<option value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';   
                                }
                            }
                                
                        }
                        echo "</select>";
                    ?>    
                  </div>
                </div>
              </div>
            <div class="row">

            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <!-- <label>Active</label><br/>
                <input type="checkbox"  name="is_active" value=""> -->
                <br/>
                <div class="custom-control custom-checkbox">
                <?php 
                    if($categoryList->isactive)
                    {
                        echo "<input type='checkbox' checked name='is_active' class='custom-control-input' id='customCheck1'>";
                    }
                    else
                    {
                        echo "<input type='checkbox' name='is_active' class='custom-control-input' id='customCheck1'>";
                    }
                ?>
                <label class="custom-control-label" for="customCheck1">Active</label>
                </div>
                </div>
               
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Update</button>
                <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$categoryList->category_id','$categoryList->category_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                <a href='<?php echo url("/manage-category");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Category</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteCategory()" class="btn btn-danger" data-dismiss="modal">Delete Category</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#categories').addClass("active");
    });
    var categoryId ="", categoryname="";
  function confirmDelete(category_id , recordName)
    {
        categoryId = category_id;
        categoryname = recordName;
    }

    function deleteCategory()
    {
        if(categoryId != "" && categoryname !="")
        {
            redurl = "/delete-category/"+categoryId+"/"+categoryname;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop