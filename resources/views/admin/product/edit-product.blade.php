@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-product");?>'>Manage Product</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$productList->product_name}}</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Edit Product</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../update-product/{{$productList->product_id}}" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Name</label>
                <input type="text" class="form-control" name="product_name" required placeholder="product Name" value='{{$productList->product_name}}'>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Product Price</label>
                <input type="text" class="form-control" name="product_price" required placeholder="product price" value='{{$productList->product_price}}'>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <!-- <div class="form-group"> -->
                <label>product Image</label>
                <img class="form-control" style=width:150px;height:150px src="../{{$productList->image}}" />
                 <input type="file" name="product_image" class="form-control" id="product_image"  >
                <!-- </div> -->
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product code</label>
                <input type="text" class="form-control" name="product_code" required placeholder="product code" value='{{$productList->product_code}}'>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Bundle Size</label>
                <input type="text" class="form-control" name="bundle_size" required placeholder="bundle size" value='{{$productList->bundle_size}}'>
                </div>
            </div>
            </div>
               <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Product Description</label>
                <input type="text" class="form-control" name="product_description" required placeholder="product description" value='{{$productList->product_description}}'>
                </div>
            </div>
            
               <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Quantity</label>
                <input type="text" class="form-control" name="quantity_in_bundle" required placeholder="quantity" value='{{$productList->quantity_in_bundle}}'>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Weight</label>
                <input type="text" class="form-control" name="weight" required placeholder="weight" value='{{$productList->weight}}'>
                </div>
            </div>
            
            </div>
            
            <div class="row">
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <!-- <label>Active</label><br/>
                <input type="checkbox"  name="is_active" value=""> -->
                <br/>
                <div class="custom-control custom-checkbox">
                <?php 
                    if($productList->isactive)
                    {
                        echo "<input type='checkbox' checked name='is_active' class='custom-control-input' id='customCheck1'>";
                    }
                    else
                    {
                        echo "<input type='checkbox' name='is_active' class='custom-control-input' id='customCheck1'>";
                    }
                ?>
                <label class="custom-control-label" for="customCheck1">Active</label>
                </div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Update</button>
                <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$productList->product_id','$productList->product_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                <a href='<?php echo url("/manage-product");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>

    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Product</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteProduct()" class="btn btn-danger" data-dismiss="modal">Delete Product</button>
           
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var productId ="", productname="";
    $(document).ready(function() {
      $('#products').addClass("active");
    });
    function confirmDelete(product_id , recordName)
    {
        productId = product_id;
        productname = recordName;
    }

    function deleteProduct()
    {
        if(productId != "" && productname !="")
        {
            redurl = "/delete-product/"+productId+"/"+productname;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop