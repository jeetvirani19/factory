@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-product");?>'>Manage Product</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$productList->product_name}}</a>
@stop
@section('main-content')
  <div class="content">
    <!-- <div class="container-fluid">
        <h1 class="navbar-brand" href="#">Welcome User!</h1>
    </div> -->
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header">
            <h5 class="card-title">View Product - {{$productList->product_name}}</h5>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Product name</label>
                    <label class="form-control">{{$productList->product_name}} </label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Price</label>
                    <label class="form-control">{{$productList->product_price}}</label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Bundle Size</label>
                    <label class="form-control">{{$productList->bundle_size}}</label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Quantity</label>
                    <label class="form-control">{{$productList->quantity_in_bundle}}</label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Weight</label>
                    <label class="form-control">{{$productList->weight}}</label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Description</label>
                    <label class="form-control">{{$productList->product_description}}</label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label>Active</label>
                    <label class="form-control">
                      <?php 
                      if($productList->isactive) { echo "Yes";}
                      else {echo "No";}
                      ?>
                      
                    </label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Code</label>
                    <label class="form-control">{{$productList->product_code}}</label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Image</label>
                    <img class="form-control" style=width:150px;height:150px src="../{{$productList->image}}" />
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-12">
                     <a href='<?php echo url("/edit-product/{$productList->product_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                    <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$productList->product_id','$productList->product_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                    <a href='<?php echo url("/manage-product");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
  </div>

    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Product</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteProduct()" class="btn btn-danger" data-dismiss="modal">Delete Product</button>
           
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var productId ="", productname="";
    $(document).ready(function() {
      $('#products').addClass("active");
    });
    function confirmDelete(product_id , recordName)
    {
        productId = product_id;
        productname = recordName;
    }

    function deleteProduct()
    {
        if(productId != "" && productname !="")
        {
            redurl = "/delete-product/"+productId+"/"+productname;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop