@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-product");?>'>Manage Products</a>
@stop
@section('main-content')
    <div class="content">
      <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-flex">
                    <h4 class="card-title"> Products</h4>
                    <a href='<?php echo url("/create-product/{}");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Create product</a> 
                    </div>
                    
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                            <thead class=" text-primary">
                                <th>
                                Product Name
                                </th>
                                <th>
                                Quantity
                                </th>
                                <th>
                                Price
                                </th>
                                <th class="text-center">
                                Active
                                </th>
                                <th class="text-center">
                                Actions
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $productList as $product )
                                <tr>
                                <td>
                                    {{$product->product_name}} 
                                </td>
                                <td>
                                    {{$product->quantity_in_bundle}} 
                                </td>
                                <td>
                                    {{ $product->product_price }}
                                </td>
                                <td class="text-center">
                                <?php if($product->isactive)
                                      {echo '<i class="is-active-check nc-icon nc-check-2"></i>';}
                                      else
                                      {echo '<i class="is-active-check nc-icon nc-simple-remove"></i>';}
                                  ?>
                                </td>
                                <td class="text-center">
                                    <a href='<?php echo url("/manage-product/{$product->product_id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-glasses-2"></i>&nbsp;View</a>
                                    <a href='<?php echo url("/edit-product/{$product->product_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                   <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$product->product_id','$product->product_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                   
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Product</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteProduct()" class="btn btn-danger" data-dismiss="modal">Delete Product</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var productId ="", productname="";
    $(document).ready(function() {
      $('#products').addClass("active");
    });
    function confirmDelete(product_id , recordName)
    {
        productId = product_id;
        productname = recordName;
    }

    function deleteProduct()
    {
        if(productId != "" && productname !="")
        {
            redurl = "/delete-product/"+productId+"/"+productname;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop