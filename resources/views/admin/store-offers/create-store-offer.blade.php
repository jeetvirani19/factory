@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-store/$storeId");?>'>Store</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href='#'>&nbsp;Create New Store Offer</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create Store Offer</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" onsubmit="return checkStoreOfferProduct()" action="../save-store-offers">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Store Offer Name</label>
                <input type="text" class="form-control" name="store_name" required placeholder="Store Offer Name" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <div class="custom-control custom-checkbox">
                <br/><br/>
                <input type='checkbox' name='is_published' class='custom-control-input' id='is_published'>
                <label class="custom-control-label" for="is_published">Publish Offer</label>
                </div>
            </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Offer Start Date</label>
                <input type="text" class="form-control" name="offer_start_date_dp" id="offer_start_date_dp" required placeholder="Offer Start Date" >
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Offer End Date</label>
                <input type="text" class="form-control" name="offer_end_date_dp" id="offer_end_date_dp" required placeholder="Offer End Date" >
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="store_products" id="store_products" value="">
                <input type="hidden" name="store_id" id="store_id" value="<?php echo $storeId;?>">
            </div>
            </div>
            <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <thead class=" text-primary">
                                <th>Product Name</th>
                                <th>Product Price</th>
                                <th>Product Offer Price</th>
                                <th>Remove</th>
                    </thead>
                    <tbody id="product-list">
                    </tbody>
                </table>
            <ul id="myUL">
            </ul>
            </div>    
            <div class="col-md-12">
                <a href='#' data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Add Products</a> 
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
                <a href='<?php echo url("/manage-store");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>


<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Add Products</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <div class="container"> 
            <div class="col-md-12 px-1">
                <div class="form-group">
                <label>Products</label>
                    <?php
                    echo "<select onchange='changeProductdata()' name='selected_product' id='selected_product' class='form-control'>";
                    echo '<option value="0">Select Product</option>';
                    for($i = 0; $i < count($productsList); $i++)
                    {                
                        echo '<option product_price="'.$productsList[$i]->product_new_price.'" product="'.$productsList[$i]->product_name.'" value="'.$productsList[$i]->product_id.'">'.$productsList[$i]->product_name.'</option>';                            
                    }
                    echo "</select>";
                    ?>      
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <label>&nbsp;Product Name : &nbsp;</label>
                    <label id="product_name"></label>
                </div>
            </div>
            <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-5 px-1">
                        <div class="form-group">
                            <label>Product Price</label>
                            <input type="number" class="form-control" name="product_price" id="product_price" required placeholder="Product Price" >
                        </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                        <div class="form-group">
                            <label>Product Offer Price</label>
                            <input type="number" class="form-control" name="new_product_price" id="new_product_price" required placeholder="Product Offer Price" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="newElement()" class="btn btn-success" data-dismiss="modal">Add Product</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>

@stop
@section('extended-files')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>
    function checkStoreOfferProduct()
    {
        debugger;
        if(selectedProductsJSON && selectedProductsJSON.products && selectedProductsJSON.products.length == 0)
        {
            showNotification('top' , 'right' , 'Please add minimum 1 products to this offer!')
            return false;
        }
        else
        {
            return true;
        }
    }
    var selectedProductsJSON = {
       products : []
    };
    function createTableData( dataElement )
    {
        var tableDataElem = document.createElement("td");
        tableDataElem.innerHTML = dataElement;
        return tableDataElem;
    }
    function newElement() {

        var selected_product_os = document.getElementById("selected_product"); 
        var selected_index = selected_product_os.options[selected_product_os.selectedIndex];
        var product_name = selected_index.getAttribute('product');
        var product_price = document.getElementById("product_price").value;
        var product_offer_price = document.getElementById("new_product_price").value;

        tableRowElem = document.createElement("tr");
        tableRowElem.appendChild(createTableData( product_name ));
        tableRowElem.appendChild(createTableData( product_price ));
        tableRowElem.appendChild(createTableData( product_offer_price ));

        document.getElementById("product-list").appendChild(tableRowElem);
        selectedProductsJSON.products.push({
            product_id : selected_index.value,
            new_product_price : product_price,
            old_product_price : product_offer_price
        });
        document.getElementById('store_products').value=JSON.stringify(selectedProductsJSON);
        var span = document.createElement("SPAN");
        var txt = document.createTextNode("\u00D7");
        span.className = "close";
        span.appendChild(txt);
        var closeTd = document.createElement("td");
        closeTd.appendChild(span);
        tableRowElem.appendChild(closeTd);
        tableRowElem.setAttribute("elemId" , (selectedProductsJSON.products.length) - 1);
        closeTd.onclick = function() {
            var div = this.parentElement;
            div.style.display = "none";
            if(div.hasAttribute('elemId'))
            {
                if(selectedProductsJSON.products.length == 1)
                {
                    selectedProductsJSON.products = [];    
                }
                else
                {
                    selectedProductsJSON.products = selectedProductsJSON.products.splice(div.getAttribute('elemId') , 1);
                }
                debugger;
                console.log(selectedProductsJSON);
                document.getElementById('store_products').value=JSON.stringify(selectedProductsJSON);
            }
         }
        }
        function changeProductdata()
        {
            var selected_product_os = document.getElementById("selected_product"); 
            var selected_index = selected_product_os.options[selected_product_os.selectedIndex];
            var product_name = selected_index.getAttribute('product');
            var product_price = selected_index.getAttribute('product_price');
            document.getElementById("product_price").value = product_price;
            document.getElementById("product_name").innerHTML = product_name;
        }

    $(document).ready(function() {
      $('#stores').addClass("active");
      $('#offer_start_date_dp').datepicker({
           uiLibrary: 'bootstrap4'
      });
      $('#offer_end_date_dp').datepicker({
           uiLibrary: 'bootstrap4'
      });
      
    });
</script>

@stop