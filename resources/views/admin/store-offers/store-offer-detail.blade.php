@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-store/{$storeOffersQuery->store_id}");?>'>Manage Store Offers</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">{{$storeOffersQuery->store_offer_name}}</a>
@stop
@section('main-content')
  <div class="content">
    <!-- <div class="container-fluid">
        <h1 class="navbar-brand" href="#">Welcome User!</h1>
    </div> -->
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header" style="display:flex;padding-left:0px;">
            <h5 class="card-title" style="width : 100%;">{{$storeOffersQuery->store_offer_name}}</h5>
            <a href='<?php echo url("/edit-store-offers/{$storeOffersQuery->offer_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
            <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$storeOffersQuery->offer_id' , '$storeOffersQuery->store_id' , '$storeOffersQuery->store_offer_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
            <a href='<?php echo url("/manage-store/{$storeOffersQuery->store_id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Offer Name</label>
                    <label class="form-control">{{$storeOffersQuery->store_offer_name}}</label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Is Published</label>
                    <label class="form-control"><?php
                      if($storeOffersQuery->is_published){echo "Yes"; }else {echo "No"; }
                    ?></label>
                  </div>
                </div>
               </div>
               <div class="row"> 
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Offer Start Date</label>
                    <label class="form-control">
                    <?php echo  date("F j, Y, g:i A", strtotime($storeOffersQuery->offer_start_date));?>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Offer End Date</label>
                    <label class="form-control">
                    <?php echo  date("F j, Y, g:i A", strtotime($storeOffersQuery->offer_end_date));?>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
              <h5 class="card-title" style="width : 100%;">Store Products - (<?php echo count($storeOfferProductsQuery)?>) </h5>
              </div>
               <div class="row"> 
               <div class="inner-table-responsive table-responsive">
                            <table id="offer-product-list" class="table">
                            <thead class=" text-primary">
                                <th>
                                Product Image
                                </th>
                                <th>
                                Product Name
                                </th>
                                <th>
                                Old Price
                                </th>
                                <th>
                                New Price
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $storeOfferProductsQuery as $storeOfferProduct )
                                <tr>
                                <td>
                                  <img class="img-responsive" height="50" width="50" src='<?php echo url('/').'/'.$storeOfferProduct->image;?>' />
                                </td>
                                <td>
                                    {{ $storeOfferProduct->product_name}}
                                </td>
                                <td>
                                    {{ $storeOfferProduct->new_product_price}}
                                </td>
                                <td>
                                    {{ $storeOfferProduct->old_product_price}}
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
              </div>
            </form>
          </div>
        </div>
  </div>
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Store Offer {{$storeOffersQuery->store_offer_name}}?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteStore()" class="btn btn-danger" data-dismiss="modal">Delete Store Offer</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" ></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js" ></script>
<script>
    $(document).ready(function() {
      $('#offer-product-list').DataTable();
      $('#stores').addClass("active");
    });
    var storeOfferId ="" ,storId ="", storeOfferName="";
    function confirmDelete(recordId , storeId , recordName)
    {
        storeOfferId = recordId;
        storId = storeId;
        storeOfferName = recordName;
    }

    function deleteStore()
    {
        if(storeOfferId != "" && storId != "" && storeOfferName !="")
        {
            redurl = "/delete-store-offers/"+storId+"/"+storeOfferId+"/"+storeOfferName;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>

@stop