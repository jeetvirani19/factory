@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-store/{$storeId}");?>'>{{ $storeQuery->store_name }}</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">Manage Store Offers</a>
@stop
@section('main-content')
    <div class="content">
      <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-flex">
                    <h4 class="card-title"> Store Offers for {{ $storeQuery->store_name }}</h4>
                    <a href='<?php echo url("/create-store-offers/{$storeId}");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Create New Offer</a> 
                    <a href='<?php echo url("/manage-store/{$storeId}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                    </div>
                    <div class="card-body">
                        <?php
                            if( count($storeOffersList) == 0 )
                            {
                                echo "&nbsp;<h5 class='h5 text-muted'>No store offers</h5>";
                            }
                            else
                            {
                        ?>
                        
                        <!-- <p class="lean">No store offers</p> -->
                        <div class="table-responsive">
                            <table class="table">
                            <thead class=" text-primary">
                                <th>
                                Offer Name
                                </th>
                                <th>
                                Offer Start Date
                                </th>
                                <th>
                                Offer End Date
                                </th>
                                <th class="text-center">
                                Offer Published
                                </th>
                                <th class="text-center">
                                Actions
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $storeOffersList as $storeOffer )
                                <tr>
                                <td>
                                    {{ $storeOffer->store_offer_name}}
                                </td>
                                <td>
                                    <?php echo  date("F j, Y, g:i A", strtotime($storeOffer->offer_start_date));?>
                                    
                                </td>
                                <td>
                                    <?php echo  date("F j, Y, g:i A", strtotime($storeOffer->offer_end_date));?>
                                </td>
                                <td class="text-center">
                                <?php if($storeOffer->is_published){echo '<i class="is-active-check nc-icon nc-check-2 text-center"></i>';}  ?>
                                </td>
                                <td class="text-center">
                                    <a href='<?php echo url("/view-store-offer-detail/{$storeOffer->offer_id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-glasses-2"></i>&nbsp;View</a>
                                    <br/><a href='<?php echo url("/edit-store-offers/{$storeOffer->offer_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                    <br/><a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$storeOffer->offer_id' , '$storeId' , '$storeOffer->store_offer_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
                        <?php };?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Store Offer {{$storeQuery->store_name}}?</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteStore()" class="btn btn-danger" data-dismiss="modal">Delete Store Offer</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
@section('extended-files')
<script>
    var storeOfferId ="" ,storId ="", storeOfferName="";
    $(document).ready(function() {
      $('#stores').addClass("active");
    });
    function confirmDelete(recordId , storeId , recordName)
    {
        storeOfferId = recordId;
        storId = storeId;
        storeOfferName = recordName;
    }

    function deleteStore()
    {
        if(storeOfferId != "" && storId != "" && storeOfferName !="")
        {
            redurl = "/delete-store-offers/"+storId+"/"+storeOfferId+"/"+storeOfferName;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>

@stop