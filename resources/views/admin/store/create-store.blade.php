@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='#'>Create New Store</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create Store</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="./save-store" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Store Name</label>
                <input type="text" class="form-control" name="store_name" required placeholder="Store Name" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Store Address</label>
                <textarea rows="4" class="form-control" name="store_address" cols="50">
                </textarea>
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Whatsapp Number</label>
                <input type="text" class="form-control" name="whatsapp_no" placeholder="Whatsapp Number" >
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Facebook URL</label>
                <input type="text" class="form-control" name="fb_url" placeholder="Facebook URL" >
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Twitter URL</label>
                <input type="text" class="form-control" name="twitter_url" placeholder="Twitter URL" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <label>Store Image</label>
             
                  <input type="file" name="store_image" class="form-control" id="store_image" required >
            </div>
            
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Area</label>
                <?php
                    echo "<select name='location_id' class='form-control'>";
                    for($i = 0; $i < count($locations); $i++)
                    {
                        if( Auth::user()->user_type == 1 )
                        {
                            if(Auth::user()->location_id == $locations[$i]->location_id)
                            {
                                echo '<option value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';   
                            }
                        }
                        else
                        {
                            echo '<option value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';   
                        }
                            
                    }
                    echo "</select>";
                ?>    
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
                <!-- <button class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Cancel</button> -->
                <a href='<?php echo url("/manage-store");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#stores').addClass("active");
    });
</script>

@stop