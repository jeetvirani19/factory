@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-store");?>'>Manage Store</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">{{$store->store_name}}</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Edit Store</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../update-store/{{$store->store_id}}" enctype="multipart/form-data">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Store Name</label>
                <input type="text" class="form-control" name="store_name" required placeholder="Store Name" value='{{$store->store_name}}'>
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Whatsapp Number</label>
                <input type="text" class="form-control" name="whatsapp_no" placeholder="Whatsapp No" value='{{$store->whatsapp_no}}'>
                </div>
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Facebook URL</label>
                <input type="text" class="form-control" name="fb_url" placeholder="Facebook URL" value='{{$store->fb_url}}'>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <label>Store Image</label>
                <img class="form-control" style=width:150px;height:150px src="../{{$store->image}}" />
                 <input type="file" name="store_image" class="form-control" id="store_image"  >
            </div>
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Twitter URL</label>
                <input type="text" class="form-control" name="twitter_url" placeholder="Twitter URL" value='{{$store->twitter_url}}'>
                </div>
            </div>
            
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Area</label>
                <?php
                    echo "<select name='location_id' class='form-control'>";
                for($i = 0; $i < count($locations); $i++)
                    {
                        if($locations[$i]->area == $store->area)
                        {
                            echo '<option value="'.$locations[$i]->location_id.'" selected >'.$locations[$i]->area.'</option>';
                        }
                        else
                        {
                            if( Auth::user()->user_type != 1 )
                            {
                              echo '<option value="'.$locations[$i]->location_id.'">'.$locations[$i]->area.'</option>';
                            }
                        }
                        
                    }
                    echo "</select>";
                ?>    
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            </div>
            <div class="row">
            <div class="col-md-offset-1 col-md-10 pl-1">
                <div class="form-group">
                <label>Store Address</label>
                <textarea rows="4" class="form-control" name="store_address" cols="50">{{$store->store_address}}</textarea>
                </div>
            </div></div>
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Update</button>
                <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$store->store_id' , '$store->store_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                <a href='<?php echo url("/manage-store");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Store</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label class="col-lg-12" id="deleteLabel">Delete Store ?<br/></label>
                <br/><label class="col-lg-12">This will delete the associated store offers as well</label>
                </div>
                <div class="modal-footer">
                <button type="button" onclick="deleteStore()" class="btn btn-danger" data-dismiss="modal">Delete Store</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
@stop
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#stores').addClass("active");
    });
    var storId ="", storName="";
    function confirmDelete( storeId , storeName)
    {
        storId = storeId;
        storName = storeName;
        document.getElementById("deleteLabel").innerHTML = "Delete store "+storName+"?";
    }

    function deleteStore()
    {
        if( storId != "" && storName !="")
        {
            redurl = "/delete-store/"+storId+"/"+storName;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>

@stop