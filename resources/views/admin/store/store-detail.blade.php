@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-store");?>'>Manage Store</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$store->store_name}}</a>
@stop
@section('main-content')
  <div class="content">
    <!-- <div class="container-fluid">
        <h1 class="navbar-brand" href="#">Welcome User!</h1>
    </div> -->
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header" style="display:flex;">
            <h5 style="width:100%;" class="card-title">View Store - {{$store->store_name}}</h5>
            <a style="float:right;" href='<?php echo url("/view-store-offers/{$store->store_id}/{$store->store_name}");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-settings"></i>&nbsp;Manage Offers</a>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Store Name</label>
                    <label class="form-control">{{$store->store_name}}</label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Whatsapp Number</label>
                    <label class="form-control">{{$store->whatsapp_no}}</label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Facebook URL</label>
                    <label class="form-control">{{$store->fb_url}}</label>
                  </div>
                </div>
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Twitter URL</label>
                    <label class="form-control">{{$store->twitter_url}}</label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Store Area</label>
                    <label class="form-control">{{$store->area}}</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-10 px-1">
                  <div class="form-group">
                    <label>Store Address</label>
                    <label class="form-control">
                       {{$store->store_address}}
                    </label>
                  </div>
                   <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Store Image</label>
                    <img class="form-control" style=width:150px;height:150px src="../{{$store->image}}" />
                  </div>
                </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <a href='<?php echo url("/edit-store/{$store->store_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                  <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$store->store_id' , '$store->store_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                  <a href='<?php echo url("/manage-store");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
  </div>
  <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Store</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label class="col-lg-12" id="deleteLabel">Delete Store ?<br/></label>
                <br/><label class="col-lg-12">This will delete the associated store offers as well</label>
                </div>
                <div class="modal-footer">
                <button type="button" onclick="deleteStore()" class="btn btn-danger" data-dismiss="modal">Delete Store</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
@stop  
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#stores').addClass("active");
    });
    var storId ="", storName="";
    function confirmDelete( storeId , storeName)
    {
        storId = storeId;
        storName = storeName;
        document.getElementById("deleteLabel").innerHTML = "Delete store "+storName+"?";
    }

    function deleteStore()
    {
        if( storId != "" && storName !="")
        {
            redurl = "/delete-store/"+storId+"/"+storName;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>

@stop