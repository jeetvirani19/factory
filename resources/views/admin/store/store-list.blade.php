@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-store");?>'>Manage Store</a>
@stop
@section('main-content')
    <div class="content">
      <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-flex">
                    <h4 class="card-title"> Stores</h4>
                    <a href='<?php echo url("/create-store");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Create New Store</a> 
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                            <thead class=" text-primary">
                                <th>
                                Name
                                </th>
                                <th>
                                Area
                                </th>
                                <th class="address text-center">
                                Address
                                </th>
                                <th class="text-center">
                                Actions
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $storeList as $store )
                                <tr>
                                <td>
                                    {{$store->store_name}}
                                </td>
                                <td>
                                    {{ $store->area }}
                                </td>
                                <td class="text-center">
                                {{ $store->store_address }}
                                </td>
                                <td class="text-center">
                                    <a href='<?php echo url("/manage-store/{$store->store_id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-glasses-2"></i>&nbsp;View</a>
                                    <a href='<?php echo url("/edit-store/{$store->store_id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                    <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$store->store_id' , '$store->store_name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-xs">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Store</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label class="col-lg-12" id="deleteLabel">Delete Store ?<br/></label>
                <br/><label class="col-lg-12">This will delete the associated store offers as well</label>
                </div>
                <div class="modal-footer">
                <button type="button" onclick="deleteStore()" class="btn btn-danger" data-dismiss="modal">Delete Store</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#stores').addClass("active");
    });
    var storId ="", storName="";
    function confirmDelete( storeId , storeName)
    {
        storId = storeId;
        storName = storeName;
        document.getElementById("deleteLabel").innerHTML = "Delete store "+storName+"?";
    }

    function deleteStore()
    {
        if( storId != "" && storName !="")
        {
            redurl = "/delete-store/"+storId+"/"+storName;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>

@stop