@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href="#">Create Admin</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Create Admin</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../save-admin">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="first_name" required placeholder="Name" >
                </div>
            </div>
         
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Email Id</label>
                <input type="text" class="form-control" name="user_email" required placeholder="Email Id" >
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <!-- <label>Active</label><br/>
                <input type="checkbox"  name="is_active" value=""> -->
                <br/>
                <div class="custom-control custom-checkbox">
                  
                        <input type='checkbox' name='is_active' class='custom-control-input' id='customCheck1'>
                   
                <label class="custom-control-label" for="customCheck1">Active</label>
                </div>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" required placeholder="password" >
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Save</button>
                <a href='<?php echo url("/manage-admin");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>
@stop
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#admins').addClass("active");
    });
</script>

@stop