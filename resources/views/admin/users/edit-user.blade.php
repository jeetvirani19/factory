@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-admin");?>'>Manage Admin</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;Edit - {{$admin->name}}</a>
@stop
@section('main-content')
<div class="content">
<div class="row">
<div class="card card-user col-lg-12">
        <div class="card-header">
        <h5 class="card-title">Edit Admin</h5>
        </div>
        <div class="card-body col-lg-12">
        <form method="POST" action="../update-admin/{{$admin->id}}">
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="first_name" required placeholder="Name" value='{{$admin->name}}'>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                <div class="form-group">
                <label>Email Id</label>
                <input type="text" class="form-control" name="user_email" required placeholder="Email Id" value='{{$admin->email}}'>
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5 pl-1">
                <div class="form-group">
                <!-- <label>Active</label><br/>
                <input type="checkbox"  name="is_active" value=""> -->
                <br/>
                <div class="custom-control custom-checkbox">
                <?php 
                    if($admin->is_active)
                    {
                        echo "<input type='checkbox' checked name='is_active' class='custom-control-input' id='customCheck1'>";
                    }
                    else
                    {
                        echo "<input type='checkbox' name='is_active' class='custom-control-input' id='customCheck1'>";
                    }
                ?>
                <label class="custom-control-label" for="customCheck1">Active</label>
                </div>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-5 px-1">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <!-- <button class="btn btn-success btn-round"><i class="nc-icon nc-refresh-69"></i>&nbsp;Rest Password</button> -->
                <button type="submit" class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Update</button>
                <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$admin->id','$admin->name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                 <a href='<?php echo url("/manage-admin");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
            </div>
            </div>
        </form>
        </div>
    </div>
</div>

   <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Admin</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteAdmin()" class="btn btn-danger" data-dismiss="modal">Delete Admin</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#admins').addClass("active");
    });
    var adminId ="", name="";
  function confirmDelete(id , recordName)
    {
        adminId = id;
        name = recordName;
    }

    function deleteAdmin()
    {
        if(adminId != "" && name !="")
        {
            redurl = "/delete-admin/"+adminId+"/"+name;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop
     