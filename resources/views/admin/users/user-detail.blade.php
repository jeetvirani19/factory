@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href='<?php echo url("/manage-admin");?>'>Manage Admin</a><i class="nc-icon nc-minimal-right"></i>
<a class="navbar-brand" href="#">&nbsp;{{$admin->name}}</a>
@stop
@section('main-content')
  <div class="content">
    <!-- <div class="container-fluid">
        <h1 class="navbar-brand" href="#">Welcome User!</h1>
    </div> -->
    <div class="row">
    <div class="card card-user col-lg-12">
          <div class="card-header">
            <h5 class="card-title">View Admin</h5>
          </div>
          <div class="card-body col-lg-12">
            <form>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Name</label>
                    <label class="form-control">{{$admin->name}} </label>
                  </div>
                </div>
                <div class="col-md-offset-1 col-md-5 pl-1">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <label class="form-control">{{$admin->email}}</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-5 px-1">
                  <div class="form-group">
                    <label>Admin Type</label>
                    <label class="form-control">
                      <?php 
                      if($admin->user_type == 1) { echo "Country Admin";}
                      else {echo "Super Admin";}
                      ?>
                    </label>
                  </div>
                  <div class="form-group">
                    <label>Active</label>
                    <label class="form-control">
                      <?php 
                      if($admin->is_active) { echo "Yes";}
                      else {echo "No";}
                      ?>
                    </label>
                  </div>
                </div>
                </div>
                </div>
                
              </div>
              <div class="row">
                <div class="col-md-12">
                    <a href='<?php echo url("/edit-admin/{$admin->id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                   <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$admin->id','$admin->name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                    <a href='<?php echo url("/manage-admin");?>' class="btn btn-success btn-round"><i class="nc-icon nc-minimal-left"></i>&nbsp;Back</a>
                </div>
              </div>
            </form>
          </div>
        </div>
  </div>

   <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Admin</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteAdmin()" class="btn btn-danger" data-dismiss="modal">Delete Admin</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#admins').addClass("active");
    });
    var adminId ="", name="";
  function confirmDelete(id , recordName)
    {
        adminId = id;
        name = recordName;
    }

    function deleteAdmin()
    {
        if(adminId != "" && name !="")
        {
            redurl = "/delete-admin/"+adminId+"/"+name;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop
     