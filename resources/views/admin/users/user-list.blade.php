@extends('admin.admin-layout')
@section('page-header-name')
<a class="navbar-brand" href="#pablo">Manage Admins</a>
@stop
@section('main-content')
    <div class="content">
      <div class="row">
        <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-flex">
                    <h4 class="card-title"> Country Admins</h4>
                    <a href='<?php echo url("/create-admin/{}");?>' class="btn btn-primary btn-round"><i class="nc-icon nc-simple-add"></i>&nbsp;Create Admin</a> 
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                            <thead class=" text-primary">
                                <th>
                                Name
                                </th>
                                <th>
                                Email
                                </th>
                                <th class="text-center">
                                Active
                                </th>
                                <th class="text-center">
                                Actions
                                </th>
                            </thead>
                            <tbody>
                            @foreach( $adminList as $admin )
                                <tr>
                                <td>
                                    {{$admin->name}}
                                </td>
                                <td>
                                    {{ $admin->email }}
                                </td>
                                <td class="text-center">
                                <?php if($admin->is_active)
                                      {echo '<i class="is-active-check nc-icon nc-check-2"></i>';}
                                      else
                                      {echo '<i class="is-active-check nc-icon nc-simple-remove"></i>';}
                                  ?>
                                </td>
                                <td class="text-center">
                                    <a href='<?php echo url("/manage-admin/{$admin->id}");?>' class="btn btn-success btn-round"><i class="nc-icon nc-glasses-2"></i>&nbsp;View</a>
                                    <a href='<?php echo url("/edit-admin/{$admin->id}");?>' class="btn btn-warning btn-round"><i class="nc-icon nc-ruler-pencil"></i>&nbsp;Edit</a>
                                   <a href="#" data-toggle="modal" data-target="#myModal" onclick="<?php echo "confirmDelete('$admin->id','$admin->name')";?>" class="btn btn-danger btn-round"><i class="nc-icon nc-simple-remove"></i>&nbsp;Delete</a>
                                </td>
                                </tr>
                            @endforeach  
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

   <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-xs">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Delete Admin</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" onclick="deleteAdmin()" class="btn btn-danger" data-dismiss="modal">Delete Admin</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
@stop  
     
@section('extended-files')
<script>
    $(document).ready(function() {
      $('#admins').addClass("active");
    });
    var adminId ="", name="";
  function confirmDelete(id , recordName)
    {
        adminId = id;
        name = recordName;
    }

    function deleteAdmin()
    {
        if(adminId != "" && name !="")
        {
            redurl = "/delete-admin/"+adminId+"/"+name;
            window.location = '<?php echo url('/');?>'+redurl;
        }
    }
    <?php 
        if(isset($deletedValue))
        {echo "showNotification('top' , 'right' , 'Offer $deletedValue deleted!')";}
    ?>
</script>
@stop
     