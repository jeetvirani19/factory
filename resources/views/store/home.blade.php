<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Grand Hyper Market</title>
    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/store/bootstrap.min.css" >
    <link rel="stylesheet" href="css/store/style.css" >
    <script src="https://use.fontawesome.com/a3a87d9273.js"></script>
    <link rel="shortcut icon" href="images/store/logo.png">
  </head>
  <body>
    <header class="">
      <nav class="navbar navbar-expand-lg navbar-light ">
        <a class="navbar-brand" href="#">
          <img class="logo" src="images/store/logo.png"  alt="Grand-Logo">
        </a>
        <button class="navbar-toggler my-2" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
          <ul class="navbar-nav ">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#about">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#event">News/Events</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#career">Careers</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#contact">Contact Us</a>
            </li>

          </ul>
        </div>
      </nav>
    </header>
    <!-- slider section starts -->
    <section class="position-relative slider py-5" style="background-image:url(images/store/slider-img.jpg); background-repeat: no-repeat;">

        <div class="container">
          <div class="row py-5 justify-content-center">
            <div class="col-md-3 px-0">
              <label class="store">Select Store</label>
              <select class="form-control" >
                <option selected>Country</option>
                <option value="1">UAE</option>
                <option value="2">Kuwait</option>
                <option value="3">India</option>
              </select>
            </div>
              <div class="col-md-3 px-0">
              <select class="form-control" id="inputGroupSelect01">
                <option selected>City</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
            </div>
            <div class="col-md-4 px-0">
              <select class="form-control" id="inputGroupSelect01">
                <option selected>Select Store</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </select>
              <button type="submit" class="store_search"><label>GO</label> </button>
            </div>
          </div>
        </div>
          
    </section>
    <!-- slider section ends -->
    <!-- about section starts -->
    <section class="py-5" id="about">
        <div class="container py-5">
          <div class="row">
            <div class="col-md-6">
                <video width="240" height="240" poster="data:image/png,AAAA" style="background:transparent no-repeat url('images/store/video-img-1.png');background-size:100%">
              
                  <source src="store/video.mp4" type="video/mp4">
                  <source src="store/video.ogg" type="video/ogg">
                  Your browser does not support the video tag.
               </video>
               <img class="about-img" src="images/store/img-2.png" />
            </div>
            <div class="col-md-6 py-5">
              <h3 class="top-head">About</h3>
              <h3 class="font-weight-bold">Grand HyperMarket </h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce et tincidunt neque. Morbi at mattis neque. Donec at facilisis urna. Vivamus lacus nisl</p>
              <p>Nunc tincidunt metus vulputate ex mollis, sed imperdiet mauris finibus. Proin bibendum lectus vel eros interdum, eget tristique dui pharetra.Proin bibendum lectus vel eros interdum, eget tristique dui pharetra. </p>
            </div>
          </div>
        </div>
    </section>
    <!-- about section ends -->

    <!-- event section start -->
    <section id="event" class="py-5">
      <div class="container py-5">
        <div class="row">
          <div class="col-md-12 text-center">
              <h3 class="top-head">Latest</h3>
              <h3 class="font-weight-bold">News/Events</h3>
              <p>Nunc tincidunt metus vulputate ex mollis, sed imperdiet mauris finibus. Proin bibendum lectus vel eros interdum, eget tristique dui pharetra. Morbi luctus tincidunt neque a sagittis. Ut scelerisque turpis dolor, pharetra porta nibh dictum eu. Nam malesuada sit amet nisl id molestie. </p>
          </div>
        </div>
          <div class="row py-5">
            <div class="col-md-6 px-4">
              <div class="row event">
                <div class="col-md-4 img-holder pl-0">
                    <img src="images/store/sq-img-1.png" class="  " alt="...">
                </div>
                <div class="col-md-8 py-4">
                    <h5>Title sub 1</h5>
                    <p>Proin bibendum lectus vel eros interdum, eget tristique dui pharetra.nibh dictum.</p>
                </div>
            </div>
            </div>
            <div class="col-md-6 px-4">
                <div class="row event">
                    <div class="col-md-4 img-holder pl-0">
                        <img src="images/store/sq-img-2.png" class="  " alt="...">
                    </div>
                    <div class="col-md-8 py-4">
                        <h5>Title sub 2</h5>
                        <p>Aenean rutrum diam malesuada dui consectetur vestibulum. Sed varius condimentum nulla vel sodales. Class aptent taciti sociosqu ad.</p>
                    </div>
                </div>
            </div>
          </div>
          <div class="row">
              <div class="col-md-6 px-4">
                  <div class="row event ">
                      <div class="col-md-4 img-holder pl-0">
                          <img src="images/store/sq-img03.png" class="  " alt="...">
                      </div>
                      <div class="col-md-8 py-4">
                          <h5>Title sub 2</h5>
                          <p>Aenean rutrum diam malesuada dui consectetur vestibulum. Sed varius condimentum nulla vel sodales. Class aptent taciti .</p>
                      </div>
                  </div>
              </div>
              <div class="col-md-6 px-4">
                  <div class="row event">
                      <div class="col-md-4 img-holder pl-0">
                          <img src="images/store/sq-img-4.png" class="  " alt="...">
                      </div>
                      <div class="col-md-8 py-4">
                          <h5>Title sub 2</h5>
                          <p>Aenean rutrum diam malesuada dui consectetur vestibulum. Class aptent taciti sociosqu ad litora torquent per conubia nostra.</p>
                      </div>
                  </div>
              </div>
            </div>

      </div>
    </section>
    <!-- event section ends -->

    <!-- career section starts -->
    <section id="career" class="py-5">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h3 class="top-head">Current</h3>
                    <h3 class="font-weight-bold">Openings</h3>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-6 text-right">
                      <img class="w-75" src="images/store/man-img.png" />
                    </div>
                    <div class="col-md-6">
                      <form class="py-5 " action="" method="POST">
                          <div class="row py-3">
                            <div class="col">
                              <select class="form-control">
                                <option value="">Select Location</option>
                              </select>
                            </div>
                          </div>
                          <div class="row py-3">
                              <div class="col">
                                <select class="form-control">
                                  <option value="">Job Role</option>
                                </select>
                              </div>
                            </div>
                            <div class="row py-3">
                                <div class="col">
                                  <input type="text" class="form-control" placeholder="Your Name" />
                                </div>
                              </div>
                              <div class="row py-3">
                                  <div class="col">
                                    <input type="tel" class="form-control" placeholder="Your Phone Number" />
                                  </div>
                                </div>
                                <div class="row py-3">
                                    <div class="col">
                                      <input type="email" class="form-control" placeholder="Your email ID" />
                                    </div>
                                  </div>
                                <div class="row py-3">
                                    <div class="col">
                                        <div class="custom-file" id="customFile">
                                            <input type="file" class="custom-file-input" id="exampleInputFile" aria-describedby="fileHelp">
                                            <label class="custom-file-label" for="exampleInputFile">
                                                Upload Resume
                                            </label>
                                        </div>        
                                      </div>
                                  </div>
                                  <div class="row py-2">
                                      <div class="col text-right">
                                          <button class="btn btn-gradiant" type="submit">Apply</button>                            
                                        </div>
                                    </div>
                      </form>
                      </div>
              </div>
        </div>
      </section>

    <!-- career section ends -->


    <!-- contact section ends -->
    <section id="contact" class="py-5" style="background-image: url('images/store/footer-img.png');background-size: 100%;">
      <div class="container px-5">
          <div class="row ">
              <div class="col-md-12 px-5 text-center">
                  <h3 class="top-head text-light text-left">Reach</h3>
                  <h3 class="font-weight-bold text-light text-left">Grand HyperMarket</h3>
              </div>
            </div>

        <div class="row">
          <div class="col-md-6 px-5">
            <form action="">
              <div class="row py-2">
                  <div class="col">
                    <input type="text" class="form-control" placeholder="Your Name" />
                  </div>
                </div>
                <div class="row py-2">
                    <div class="col">
                      <input type="tel" class="form-control" placeholder="Your Phone Number" />
                    </div>
                  </div>
                  <div class="row py-2">
                      <div class="col">
                        <input type="email" class="form-control" placeholder="Your email ID" />
                      </div>
                    </div>
                    <div class="row py-2">
                        <div class="col">
                          <select class="form-control">
                            <option>Purpose</option>
                            <option>Purpose</option>
                            <option>Purpose</option>
                          </select>
                        </div>
                      </div>
                      <div class="row py-2">
                          <div class="col text-right">
                              <button class="btn btn-gradiant" type="submit">Submit</button>                            
                            </div>
                        </div>
                  </form>
          </div>
          <div class="col-md-6 px-5 text-light">
            <div class="row py-2">
               <div class="col">
                  <h6 class="font-weight-light">Email</h6>
              <h6 class="font-weight-bold">help@grandhyper.com</h6>
               </div>
            </div>
            <div class="row py-2">
                <div class="col">
                   <h6 class="font-weight-light">WhatsApp</h6>
               <h6 class="font-weight-bold">971581151541</h6>
                </div>
                <div class="col">
                    <h6 class="font-weight-light">Toll Free</h6>
                <h6 class="font-weight-bold">800 472 63</h6>
                 </div>
             </div>
             <div class="row py-2">
                <div class="col">
                  <ul class="pl-0" style="list-style: none;">
                    <li><a class="text-light" href="">Home</a> </li>
                    <li><a class="text-light" href="">Partner</a></li>
                    <li><a class="text-light" href=""> Profile </a></li>
                    <li><a class="text-light" href=""> Contact </a></li>
                  </ul>
                </div>
                <div class="col pt-5">
                    <a class="ss-icon" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a class="ss-icon" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                    <a class="ss-icon" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                 </div>
             </div>
          </div>

        </div>
        <hr class="mt-5">
        <div class="row ">
            <div class="col text-center py-3">
                <small class="text-light">Copyright &copy; Grandhyper</small>
            </div> 
          </div>
      </div>

    </section>
    <!-- contact section ends -->




    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/store/bootstrap.min.js" ></script>
    
  </body>
</html>
