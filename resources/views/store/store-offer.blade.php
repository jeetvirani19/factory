<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Grand Hyper Market</title>
    <!-- Bootstrap CSS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/store/bootstrap.min.css" >
    <link rel="stylesheet" href="css/store/style.css" >
    <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/store/all.min.css" />
        <link rel="shortcut icon" href="images/store/logo.png">
  </head>
  <body>
    <header>
        <nav class="navbar navbar-black">
            <a class="navbar-brand" href="#">
              <img class="logo" src="images/store/logo.png"  alt="Grand-Logo">
            </a>
            <div class="navbar-text">
                Grand Hyper Market Jaber Ali, First  Al Khail Street Jebel Ali, Dubai <a class="store-change" href="#">Change Store</a>
            </div>
              
          </nav>
    </header>
    <!-- slider section starts -->
    <section class="position-relative">
        <div class="slider-counter text-light">
          <i class="fa fa-angle-left" aria-hidden="true"></i> 
            <span class="counter-value"></span>
          <i class="fa fa-angle-right" aria-hidden="true"></i> 
        </div>
        <div class="slider owl-carousel store-carousel">
            <div class="container"> 
              <div class="row">
                <div class="col-md-5 text-left pt-5">
                  <span class="badge badge-info">OFFER</span>
                  <h1>Flat</h1>
                  <h1><b>AED 16 </b> <small> OFF</small></h1>
                  <h3>on all your daily needs</h3>
                  <br />
                  <small class="position-absolute" style="bottom:0;">*Conditions apply</small>
                </div>
                <div class="col-md-7">
                  <img class="img-fluid" src="images/store/slider-img.png"/>
                </div>
              </div>
            </div>
            <div class="container"> 
                <div class="row">
                  <div class="col-md-5 text-left pt-5">
                    <span class="badge badge-info">OFFER</span>
                    <h1>Flat</h1>
                    <h1><b>AED 16 </b> <small> OFF</small></h1>
                    <h3>on all your daily needs</h3>
                    <br />
                    <small class="position-absolute" style="bottom:0;">*Conditions apply</small>
                  </div>
                  <div class="col-md-7">
                    <img class="img-fluid" src="images/store/slider-img.png"/>
                  </div>
                </div>
              </div>
              <div class="container"> 
                  <div class="row">
                    <div class="col-md-5 text-left pt-5">
                      <span class="badge badge-info">OFFER</span>
                      <h1>Flat</h1>
                      <h1><b>AED 16 </b> <small> OFF</small></h1>
                      <h3>on all your daily needs</h3>
                      <br />
                      <small class="position-absolute" style="bottom:0;">*Conditions apply</small>
                    </div>
                    <div class="col-md-7">
                      <img class="img-fluid" src="images/store/slider-img.png"/>
                    </div>
                  </div>
                </div>
                <div class="container"> 
                    <div class="row">
                      <div class="col-md-5 text-left pt-5">
                        <span class="badge badge-info">OFFER</span>
                        <h1>Flat</h1>
                        <h1><b>AED 16 </b> <small> OFF</small></h1>
                        <h3>on all your daily needs</h3>
                        <br />
                        <small class="position-absolute" style="bottom:0;">*Conditions apply</small>
                      </div>
                      <div class="col-md-7">
                        <img class="img-fluid" src="images/store/slider-img.png"/>
                      </div>
                    </div>
                  </div>
                  <div class="container"> 
                      <div class="row">
                        <div class="col-md-5 text-left pt-5">
                          <span class="badge badge-info">OFFER</span>
                          <h1>Flat</h1>
                          <h1><b>AED 16 </b> <small> OFF</small></h1>
                          <h3>on all your daily needs</h3>
                          <br/>
                          <small class="position-absolute" style="bottom:0;">*Conditions apply</small>
                        </div>
                        <div class="col-md-7">
                          <img class="img-fluid" src="images/store/slider-img.png"/>
                        </div>
                      </div>
                    </div>
          </div>
          
    </section>
    <!-- slider section ends -->
    <!-- product section starts -->
    <section class="pt-5 pb-5">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center">
            <h1 class="font-weight-light">Our offer ends by <span class="color-violet "> 27<sup>th</sup> April, 2019 </span></h1>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
              <ul class="nav nav-pills nav-fill mt-5 " id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#allProduct" role="tab" aria-controls="allProduct" aria-selected="true">All Products</a>
                  </li>
                  @foreach( $categoryList as $category )
                  <li class="nav-item">
                    <a class="nav-link" id="pills-<?php echo strtolower("{$category->category_name}");?>-tab" data-toggle="pill" href="#<?php echo strtolower("{$category->category_name}");?>" role="tab" aria-controls="<?php echo strtolower("{$category->category_name}");?>" aria-selected="false">{{$category->category_name}}</a>
                  </li>
                  @endforeach 
                  <!-- <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#bakery" role="tab" aria-controls="bakery" aria-selected="false">Bakery</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-pharmacy-tab" data-toggle="pill" href="#pharmacy" role="tab" aria-controls="pharmacy" aria-selected="false">Pharmacy</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" id="pills-flora-tab" data-toggle="pill" href="#flora" role="tab" aria-controls="flora" aria-selected="false">Flora</a>
                    </li>
                  <li class="nav-item">
                      <a class="nav-link" id="pills-meat-tab" data-toggle="pill" href="#meat" role="tab" aria-controls="meat" aria-selected="false">Meat</a>
                    </li>
                  <li class="nav-item">
                      <a class="nav-link" id="pills-seafood-tab" data-toggle="pill" href="#seafood" role="tab" aria-controls="seafood" aria-selected="false">Seafood</a>
                    </li>
                  <li class="nav-item">
                      <a class="nav-link" id="pills-diary-tab" data-toggle="pill" href="#diary" role="tab" aria-controls="diary" aria-selected="false">Diary</a>
                    </li> -->
                </ul>

                <div class="tab-content" id="pills-tabContent">
                  <div class="tab-pane fade show active" id="allProduct" role="tabpanel" aria-labelledby="pills-allProduct-tab">
                      <div class="row pt-5">
                      @foreach( $productList as $product )
                          <div class="col-md-3">
                            <!-- product starts -->
                              <div class="card product" >
                                  <div class="card-header">
                                  
                                      AED {{$product->product_new_price}} <br/> <small><strike>AED {{$product->product_old_price}}</strike> </small>
                                    </div>
                                  <div class="card-body ">
                                    <p class="card-text text-center"><img src="../public/{{$product->image}}" /></p>
                                    <p class="card-text">{{$product->product_name}}</p>
                                    <span class="badge badge-info">{{$product->product_tags}}</span>
                                  </div>
                                </div>
                          </div>
                      @endforeach 
                        </div>
                       </div>
                    @foreach( $categoryList as $category )
                
                  <div class="tab-pane fade" id="<?php echo strtolower("{$category->category_name}");?>" role="tabpanel" aria-labelledby="pills-<?php echo strtolower("{$category->category_name}");?>-tab">
                      <div class="row pt-5">
                         @foreach( $productList as $product )
                           <?php if($product->category_id == $category->category_id)
                                      {echo ' <div class="col-md-3">
                            <!-- product starts -->
                              <div class="card product" >
                                  <div class="card-header">
                                  AED' .$product->product_new_price. '<br/> <small><strike>AED'  .$product->product_old_price. '</strike> </small>
                                    </div>
                                  <div class="card-body ">
                                    <p class="card-text text-center"><img src="../public/' .$product->image.'" /></p>
                                    <p class="card-text">' .$product->product_name. '</p>
                                    <span class="badge badge-info">' .$product->product_tags. '</span>
                                  </div>
                                </div>
                          </div>';}
                                      else
                                      {echo '<i class="is-active-check nc-icon nc-simple-remove"></i>';}
                                  ?>
                         
                      @endforeach 
                      </div>
                  </div>
                  @endforeach  
               </div>
        </div>
      </div>
    </section>
    <!-- product section ends -->
    <!-- WhatsApp section starts -->
    <section class="contact pt-5 pb-5">
       <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-5">
              <a class="btn btn-block btn-success" href="#" ><img src="images/store/whatsapp-logo.png"/> Subscribe to <b>offer</b></a>
          </div>
        </div>
        </div>
    </section>
    <!-- WhatsApp section ends -->

    <section class="offerbanner pb-5 pt-5" style="background: url(images/store/footer-bg-2.png)">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
              <img class="img-fluid position-absolute d-sm-none d-md-block"  src="images/store/footerbg-3.png" />
          </div>
          <div class="col-md-5 text-light pl-5 pt-5 pb-5">
              <h1 class="offer-title">Limelight <br> Offer for <br/> Today</h1>
              <h1 class="text-brown">AED 39.95</h1>
              <h2 class="text-brown"><strike>AED 44</strike></h2>
          </div>
        </div>
      </div>

    </section>
<section class="footer pt-5 pb-5">
  <div class="container pt-5 ">
    <div class="row justify-content-center" >
      <div class="col-md-4">
        <h4>EMAIL</h4>
        <h4><b>help@grandhyper.com</b></h4>
      </div>
      <div class="col-md-4">
          <h4>WhatsApp</h4>
          <h4><b>971581151541</b></h4>
      </div>
      <div class="col-md-4">
          <h4>Tolll Free</h4>
          <h4><b>971581151541</b></h4>
      </div>
    </div>
  </div>
</section>

<div class="share position-fixed text-center">
  <h6><b>Share offers</b></h6>
  <a class="" href="#"><img src="images/store/facebook.png"/> </a> 
  <a href="#"><img src="images/store/instagram.png"/> </a>
  <a href="#"><img src="images/store/twitter.png"/> </a>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/store/bootstrap.min.js" ></script>
    <script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js" ></script>
    <script>

var owl = $('.slider').on('initialized.owl.carousel changed.owl.carousel', function(e) {
    if (!e.namespace)  {
      return;
    }
    var carousel = e.relatedTarget;
    $('.counter-value').text(carousel.relative(carousel.current()) + 1 + '/' + carousel.items().length);
  }).owlCarousel({
    items: 1,
    loop:true,
    margin:0,
   autoplay:true
  });
  $(".fa-angle-right").click(function(){
    $('.owl-next').click();
  })
  $(".fa-angle-left").click(function(){
    $('.owl-prev').click();
  })
    </script>
    
  </body>
</html>
