<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can     ter web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', function () {
    return view('store.home');  
});
// Route::get('/store-offer', function () {
//     return view('store.store-offer');  
// });

Route::get('/', function () {
    return redirect('login');  
});


Route::get('/logout' , function(){
    Auth::logout();
    return redirect('/');  
});

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard','HomeController@getDashboard');
    Route::get('/store-offer','WebController@getStoreProduct');
    Route::get('/create-product/{product_id}','ProductController@createProduct');
    Route::post('/save-product','ProductController@saveProduct');
    Route::get('/manage-product', 'ProductController@getAllProductList');
    Route::get('/manage-product/{product_id}', 'ProductController@getProductInfo');
    Route::get('/edit-product/{product_id}', 'ProductController@editProductInfo');
    Route::post('/update-product/{product_id}', 'ProductController@updateProductInfo');
    Route::get('/delete-product/{product_id}/{product_name}', 'ProductController@deleteProduct');
    Route::get('/delete-category/{category_id}/{category_name}', 'ProductController@deleteCategory');
    Route::post('/upload-product','ProductController@uploadBulkProduct');

    Route::get('/manage-store/{id}', 'StoreController@getStoreInfo');
    Route::get('/manage-store/{deletedFlag?}/{deletedValue?}', 'StoreController@getAllStoreList');
    Route::get('/create-store', 'StoreController@createStore');
    Route::post('/save-store','StoreController@saveStore');
    Route::get('/edit-store/{store_id}', 'StoreController@editStoreInfo');
    Route::post('/update-store/{store_id}', 'StoreController@updateStoreInfo');
    Route::get('/delete-store/{store_id}/{store_name}', 'StoreController@deleteStore');


    Route::get('/manage-admin', 'AdminController@getAllAdminsList');
    Route::get('/manage-admin/{userId}', 'AdminController@getAdminProfile');
    Route::get('/edit-admin/{userId}', 'AdminController@editAdminProfile');
    Route::post('/update-admin/{userId}', 'AdminController@updateAdminProfile');
    Route::post('/save-admin', 'AdminController@saveAdminProfile');
    Route::get('/delete-admin/{id}/{name}', 'AdminController@deleteAdmin');
    Route::get('/create-admin/{userId}','AdminController@createAdminProfile');
    
    
    Route::get('/create-machine','MachineController@createMachine');
    Route::post('/save-machine','MachineController@saveMachine');
    Route::get('/manage-machine', 'MachineController@getAllMachineList');
    Route::get('/manage-machine/{machine_id}', 'MachineController@getMachineInfo');
    Route::get('/edit-machine/{machine_id}', 'MachineController@editMachineInfo');
    Route::post('/update-machine/{machine_id}', 'MachineController@updateMachineInfo');
    Route::get('/delete-machine/{machine_id}/{machine_name}', 'MachineController@deleteMachineInfo');

    // -----------*********<store offer routes>**********--------------
    Route::get('/view-store-offers/{storeId}/{deletedFlag?}/{deletedValue?}', 'StoreController@fetchStoreOffers');
    Route::get('/view-store-offer-detail/{storeOfferId}', 'StoreController@getStoreOfferDetail');
    Route::get('/create-store-offers/{storeId}', 'StoreController@createStoreOfferForm');
    Route::post('/save-store-offers','StoreController@saveStoreOffer');
    Route::get('/edit-store-offers/{storeOfferId}', 'StoreController@editStoreOfferInfo');
    Route::post('/update-store-offers/{storeOfferId}', 'StoreController@updateStoreOfferInfo');
    Route::get('/delete-store-offers/{store_id}/{store_offer_id}/{store_offer_name}', 'StoreController@deleteStoreOffer');
    // -----------*********<store offers>**********--------------
    
    Route::get('/manage-material', 'MaterialController@getAllMaterialList');
    Route::get('/manage-material/{material_id}', 'MaterialController@getMaterialInfo');
    Route::get('/create-material/{id}', 'MaterialController@createMaterial');
    Route::post('/save-material','MaterialController@saveMaterial');
    Route::get('/edit-material/{material_id}', 'MaterialController@editMaterialInfo');
    Route::post('/update-material/{material_id}', 'MaterialController@updateMaterialInfo');
    Route::get('/delete-material/{material_id}/{material_name}', 'MaterialController@deleteMaterial');

    Route::get('/manage-invoice', 'InvoiceController@getAllInvoiceList');
    Route::get('/manage-invoice/{invoice_id}', 'InvoiceController@getInvoiceInfo');
    Route::get('/create-invoice', 'InvoiceController@createInvoice');
    Route::post('/save-invoice','InvoiceController@saveInvoice');
    Route::get('/edit-invoice/{invoice_id}', 'InvoiceController@editInvoiceInfo');
    Route::post('/update-invoice/{invoice_id}', 'InvoiceController@updateInvoiceInfo');
    Route::get('/delete-invoice/{invoice_id}/{customer_name}', 'InvoiceController@deleteInvoice');
});



//-----





Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
